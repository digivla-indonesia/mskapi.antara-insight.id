<?php

error_reporting(1);
date_default_timezone_set('Asia/Jakarta');
$tglsiki = date("Ymd");
include "requirement-new.php";
$client_ip 		= $_SERVER['REMOTE_ADDR'];

$time_start = microtime(true);

$result["code"] 	= "05"; 	
$result["message"] 	= "Other Error [".__LINE__."]"; 	
if(!$conn)
{ 
	$result["code"] 	= "06"; 	
	$result["message"] 	= "Error Database [".__LINE__."]".mysql_error(); 
}
else
{
	$a 			= regis('a',""); // (array_key_exists('a',$_GET)) ? $_GET['a'] : "" ;
	$d 			= regis('data',""); //(array_key_exists('data',$_GET)) ? $_GET['data'] : "" ;
	$keys 		= regis('keys',""); //(array_key_exists('keys',$_GET)) ? $_GET['keys'] : "" ;
	//$uid		= regis('uid',""); //(array_key_exists('uid',$_GET)) ? $_GET['uid'] : "" ; 
	$lolos 		= true;
	
	
	//mylog("Client Data : ".$d,'msk_api'); 
	
	//checking a parameter
	if($a == "")
	{
		$result["code"] 	= "01"; 	
		$result["message"] 	= "Wrong Parameter";
		$lolos = false;
	}
	
	//checking d parameter
	if($lolos)
	{
		if($d == "")
		{
			$result["code"] 	= "01"; 	
			$result["message"] 	= "Wrong Parameter";
			$lolos = false;
		}
	}
	
	//checking Allowed IP
	/*
	if($lolos)
	{		 
		$agent_IP = check_allowed_ip($client_ip);
		if(!$agent_IP)
		{
			$result["code"] 	= "92"; 	
			$result["message"] 	= "Access Denied For Your IP [".__LINE__."]";
			$lolos = false;
		} 
	}*/
	
	//checking keys parameter
	if($lolos)
	{
		if($keys == "")
		{
			$result["code"] 	= "01"; 	
			$result["message"] 	= "Wrong Parameter";
			$lolos = false;
		}
		else
		{
			$agent = check_keys($keys);
			if(!$agent)
			{
				$result["code"] 	= "04"; 	
				$result["message"] 	= "Wrong Keys ";
				$lolos = false;
			}
		}
	} 
	
	//checking user id parameter
	/*
	if($lolos)
	{
		$tb_category_data = _DB_NAME_CLIENT_.".".$client_id;
		
		if($uid == "")
		{
			$result["code"] 	= "01"; 	
			$result["message"] 	= "Wrong Parameter";
			$lolos = false;
		}
		else
		{
			$agent = check_uid($agent,$uid);
			if(!$agent)
			{
				$result["code"] 	= "09"; 	
				$result["message"] 	= "Wrong UID ";
				$lolos = false;
			}
		}
	}
	* */
	
	//checking action code
	if($lolos)
	{
		if(!check_action_code($a))
		{
			$result["code"] 	= "02"; 	
			$result["message"] 	= "Wrong Action Code ".$a;
			$lolos = false;
		}
	}
	
	// checking data
	if($lolos)
	{
		$decode_data 	= base64_decode($d,true);
		if(!$decode_data)
		{
			$result["code"] 	= "03"; 	
			$result["message"] 	= "Wrong Data Parameter";
			$lolos = false;
		}
		else
		{ 
			$array_data		= unserialize($decode_data);
			if(!is_array($array_data))
			{
				$result["code"] 	= "03"; 	
				$result["message"] 	= "Wrong Data Parameter";
				$lolos = false;
			}
		}
	}
	
	if($lolos)
	{
		$rand_time = str_replace(".","_",microtime(true));
		//mylog("Client Data : ".var_export($array_data,true),'msk_api');
		$lib_file = "lib/".$a.".php";
		if(file_exists($lib_file))
		{
			include "lib/".$a.".php";
		}
		else
		{
			$result["code"] 	= "91"; 	
			$result["message"] 	= "System Internal Error";
			$lolos = false;
		}
	} 
	
} 
$time_end = microtime(true);
$time_div = $time_end -  $time_start;
$time_div = abs($time_div);
$result['exec_time'] =  $time_div;
$result['action'] 	 =  $a;
$result['timestamp'] 	 =  date("Y-m-d H:i:s");
$out = json_encode($result); 

mylog("".$result["exec_time"]."|".$client_ip."|".$a."|".$keys."|".$result["code"]."|".$result["message"] ,'hommie_api_'.$tglsiki);    

InsertToLog($keys,$client_id,$uid,$a,$time_div,$d,$result["code"],$out,$client_ip);
mysql_close();
exit($out);
?>
