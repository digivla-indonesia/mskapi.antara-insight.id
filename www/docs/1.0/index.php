<?php
	$client_ip 		= $_SERVER['REMOTE_ADDR'];
	$arrIP = array("103.21.219.210","192.168.10.132");
	
	if(!in_array($client_ip,$arrIP))
	{
		 echo "<meta http-equiv=\"refresh\" content=\"0;URL='access-denied.html'\" />";
	}
?>

<!DOCTYPE html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>DOCS API v1.0 Resources | Medmon Developers</title>
<script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script> 
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
<meta name="api:url" content="http://api.mediatrac.net/docs/" />
<link type="text/css" rel="stylesheet" media="all" href="base.css" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<style>
	#page{
		width:auto;
		height:auto;
		padding:10px;
		margin-left:20%;
		margin-right:20%;
	}
	.repeated{
		margin-left:20px;
		color : #D51C29;
	}
	.judul{
		text-align:center; 
		background-color:#EAEAEA;
		padding:10px;
	}
	.photo {
    width: 300px;
    text-align: center;
  }
  .photo .ui-widget-header {
    margin: 1em 0;
  }
  .imgtip {
    width: auto;
    height: auto;
    max-width: 350px;
  }
  .ui-tooltip {
    max-width: 350px;
  }
  .back {
    float:left;
    text-decoration:none;
    background-color:#B0B0B0;
    padding:5px;
    border-top-right-radius:3px;
    border-bottom-right-radius:3px;
    border-top-left-radius:20px;
    border-bottom-left-radius:20px;
    font-size:0.7em;
    font-weight:bolder;
    color:#F7F7F7;
    
  } 
</style>
<script>
  $(function() {
    $( document ).tooltip({
      items: "img, [data-img], [title]",
      content: function() {
        var element = $( this );
        if ( element.is( "[data-img]" ) ) {
          var text = element.text();
          return "<img class='imgtip' alt='" + text +
            "' src='images/" + 
            text + ".png'>";
        }
        if ( element.is( "[title]" ) ) {
          return element.attr( "title" );
        }
        if ( element.is( "img" ) ) {
          return element.attr( "alt" );
        }
      }
    });
  });
  </script>
<link rel="stylesheet" href="css3menu1/style.css" type="text/css" /><style type="text/css">._css3m{display:none}</style>
</head>
<body  id="page-docs" class="not-front not-logged-in no-sidebars">
<div id="page">
	<h1 id="title">DOCS API v1.0 Resources</h1>
	<div id="preface">
	<div id="dropdown"> 
    <ul id="css3menu1" class="topmenu">
	<li class="toproot"><a href="#" style="height:16px;line-height:16px;"><span>Jump To</span></a>
	<ul >
		<li><a href="#100">Specification</a></li>
		<li><a href="#101"><span>Basic Parameter</span></a>
		<ul>
			<li><a href="#102">Request</a></li>
			<li><a href="#103">Result</a></li>
		</ul></li>
		<li><a href="#106">Data Type</a></li>
		<li><a href="#104">Example</a></li>
		<li><a href="#105">APIs Process</a></li>
		</ul></li>
	</ul>
	</div> 	
	</div> 
	<div id="content-inner">
        <div id="content-main">
			<div class="view api-docs">
				<div class="content">
					<!-- content here -->
					<!-- Specification -->
					<table class="views-table cols-2">
						<caption>
							<a title="Specification" id="100" name="100"></a><strong>Specification</strong><p> Specification for use this API. </p></caption>
						<thead>
						<tr>
								  <th class="views-field views-field-title">
							  Resource        </th>
								  <th class="views-field views-field-body">
							  Description        </th>
							  </tr>
						</thead>
						<tbody>
							<tr class="odd views-row-first">
								<td class="views-field views-field-title">
									Host        
								</td>
								<td class="views-field views-field-body"> 
									<ul>
										<li>Let it be mistery</li> 
									</ul>
								</td>
							</tr>
							<tr class="even">
								<td class="views-field views-field-title">
									Method         
								</td>
								<td class="views-field views-field-body">
									GET
								</td>
							  </tr>
							<tr class="odd">
								<td class="views-field views-field-title">
									Encoding          
								</td>
								<td class="views-field views-field-body">
									<ol>
										<li>base64_encode</li>
										<li>serialize</li>
									</ol>
									Ex:<br>
									base64_encode( serialize( $data ) )
								</td>
							  </tr>
							<tr class="even views-row-last">
								<td class="views-field views-field-title">
									Result Type
								</td>
								<td class="views-field views-field-body">
									JSON
								</td>
							  </tr>
						  </tbody>
					</table>
					
					<!-- Basic Parameter -->
					<table class="views-table cols-2">
						<caption style="text-align:center;">
							<a title="Basic Parameter" id="101" name="101"></a><strong>Basic Parameter</strong><p> Basic Parameter is <b>mandatory</b> request parameters. </p>
						</caption>
						<caption>
							<a title="Request" id="102" name="102"></a><strong>Request</strong>
						</caption>
						<thead>
						<tr>
								  <th class="views-field views-field-title">
							  Variable        </th>
								  <th class="views-field views-field-body">
							  Description        </th>
							  </tr>
						</thead>
						<tbody>
							<tr class="odd views-row-first">
								<td class="views-field views-field-title">
									<code>a</code>        
								</td>
								<td class="views-field views-field-body"> 
									Action code of query process
								</td>
							</tr>
							<tr class="even">
								<td class="views-field views-field-title">
									<code>keys</code>     
								</td>
								<td class="views-field views-field-body">
									Keys authentication for agents
								</td>
							  </tr>
							<tr class="odd">
								<td class="views-field views-field-title">
									<code>uid</code>          
								</td>
								<td class="views-field views-field-body">
									User ID for end user
								</td>
							  </tr>
							<tr class="odd">
								<td class="views-field views-field-title">
									<code>client_id</code>          
								</td>
								<td class="views-field views-field-body">
									Client ID  
								</td>
							  </tr>
							<tr class="even views-row-last">
								<td class="views-field views-field-title">
									<code>data</code>
								</td>
								<td class="views-field views-field-body">
									the data parameter will be executed
								</td>
							  </tr>
						  </tbody>
					</table>
					<!-- Result -->
					<table class="views-table cols-2">						
						<caption>
							<a title="Result" id="103" name="103"></a><strong>Result</strong>
						</caption>
						<thead>
						<tr>
								  <th class="views-field views-field-title">
							  Variable        </th>
								  <th class="views-field views-field-body">
							  Description        </th>
							  </tr>
						</thead>
						<tbody>
							<tr class="odd views-row-first">
								<td class="views-field views-field-title">
									<code>a</code>        
								</td>
								<td class="views-field views-field-body"> 
									Action code of query process
								</td>
							</tr>
							<tr class="even">
								<td class="views-field views-field-title">
									<code>code</code>     
								</td>
								<td class="views-field views-field-body">
									Response Code , if response code value is 00 it means success, otherwise see in response code table
								</td>
							  </tr>
							<tr class="odd">
								<td class="views-field views-field-title">
									<code>message</code>          
								</td>
								<td class="views-field views-field-body">
									Results message description. <br>* see in response code table
								</td>
							  </tr> 
							<tr class="even views-row-last">
								<td class="views-field views-field-title">
									<code>data</code>
								</td>
								<td class="views-field views-field-body">
									Data results from the execution
								</td>
							  </tr>
						  </tbody>
					</table> 
					<!-- Data Type -->
					<table class="views-table cols-2">						
						<caption>
							<a title="Data Type" id="106" name="106"></a><strong>Data Type</strong>
						</caption>
						<thead>
						<tr>
								  <th class="views-field views-field-title">
							  Code        </th>
								  <th class="views-field views-field-body">
							  Description        </th>
							  </tr>
						</thead>
						<tbody>
							<tr class="odd views-row-first">
								<td class="views-field views-field-title">
									<code>@</code>        
								</td>
								<td class="views-field views-field-body"> 
									Value of variable. Exp: ‘Bank Indonesia’
								</td>
							</tr>
							<tr class="even">
								<td class="views-field views-field-title">
									<code>N</code>     
								</td>
								<td class="views-field views-field-body">
									Numeric
								</td>
							  </tr>
							<tr class="odd">
								<td class="views-field views-field-title">
									<code>A</code>          
								</td>
								<td class="views-field views-field-body">
									Ansi Char
								</td>
							  </tr> 
							<tr class="even views-row-last">
								<td class="views-field views-field-title">
									<code>S</code>
								</td>
								<td class="views-field views-field-body">
									String
								</td>
							  </tr>
							<tr class="even views-row-last">
								<td class="views-field views-field-title">
									<code>ANS</code>
								</td>
								<td class="views-field views-field-body">
									Ansi Numeric String 
								</td>
							  </tr>
							<tr class="even views-row-last">
								<td class="views-field views-field-title">
									<code class="repeated">… repeated</code>
								</td>
								<td class="views-field views-field-body">
									The data will be repeated. 
								</td>
							  </tr>
						  </tbody>
					</table>
					<!-- Example -->
					
					<table class="views-table cols-1">						
						<caption>
							<a title="Example" id="104" name="104"></a><strong>Example</strong>
						</caption> 
						<tbody>
							<tr class="odd views-row-first"> 
								<td class="views-field"> 
									<ol>
										<li>
											Define your array of data parameter.
											<p>
												<code>
<pre>
$data = <a href="http://www.php.net/manual/en/book.array.php" target="_BLANK">array</a>(
	"media_set" => xxxx,
);
</pre>
												</code>
											</p>
										</li>
										<li>
											Serialize your array of data.
											<p>
												<code>$serialize  	= <a href="http://www.php.net/manual/en/function.serialize.php" target="_BLANK">serialize</a>($data);</code>
											</p>
										</li>
										<li>
											Encode with base64_encode result number 2.
											<p>
												<code>$encode_data 	= <a href="http://www.php.net/manual/en/function.base64-encode.php" target="_BLANK">base64_encode</a>($serialize);</code>
											</p>
										</li>
										<li>
											Build your parameters.
											<p>
												<code>
<pre>
$params = <a href="http://www.php.net/manual/en/book.array.php" target="_BLANK">array</a>(
	"a" => "001",
	"keys" => "335f9512fri532f871ba1c242a268916",
	"uid" => "0101010110",
	"client_id" => "inter",
	"data" => $encode_data,
);
$send_params = <a href="http://www.php.net/manual/en/function.http-build-query.php" target="_BLANK">http_build_query</a>($params);
</pre>
												</code>
											</p>
										</li>
										<li>
											Send with GET method.
											<p>
												<code>$result = <a href="http://php.net/manual/en/function.file-get-contents.php" target="_BLANK">file_get_contents</a>($url."".$send_params);
												</code>
											</p>
										</li>
										<li>
											Decode the result APIs.
											<p>
												<code>
													$decode = <a href="http://www.php.net/manual/en/function.json-decode.php" target="_BLANK">json_decode</a>($result,1);
												</code>
											</p>
										</li>
										<li>
											Your Code. 
										</li>
									</ol>  
								</td>
							</tr> 
						</tbody>
					</table>
					
					<!-- API PROSES -->
					<table class="views-table cols-2">						
						<caption>
							<a title="APIs Process" id="105" name="105"></a><strong>APIs Process</strong>
						</caption>
						<thead>
						<tr>
								<th class="views-field views-field-title">
									Process Code
								</th> 
								<th class="views-field views-field-body">
									Description        
								</th>
							  </tr>
						</thead>
						<tbody>
							<tr class="odd views-row-first">
								<td class="views-field views-field-title">
									<b><a href="#001" data-img="data-img">001</a></b>        
								</td>
								<td class="views-field views-field-body"> 
									Query to get all list of client set for each client
								</td>
							</tr>
							<tr class="even">
								<td class="views-field views-field-title">
									<b><a href="#001I" data-img="data-img">001I</a></b>     
								</td>
								<td class="views-field views-field-body">
									Query to insert new category
								</td>
							</tr>							
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#001U"  data-img="data-img">001U</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to update category
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#001D"  data-img="data-img">001D</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to delete category
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#002" data-img="data-img">002</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get list of sub client
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#002U" data-img="data-img">002U</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to update sb client list
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#002I" data-img="data-img">002I</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to insert new sub client by client id
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#002D" data-img="data-img">002D</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to delete new sub client by client id
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#003" data-img="data-img">003</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get list of media set for each client where each media set  content one or several media
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#003I" data-img="data-img">003I</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to insert new media set
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#003U" data-img="data-img">003U</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to update media set
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#003D" data-img="data-img">003D</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to delete media set
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#004" data-img="data-img">004</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the list of media from media set
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#004U" data-img="data-img">004U</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to update sub media list
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#005" data-img="data-img" >005</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get Current and past news (3 days)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#006" data-img="data-img" >006</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to show the amount of input articles group by category in a range of a date (Visibility)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#007" data-img="data-img" >007</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to show the number of article id in particular category and media (Mentions)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#008" data-img="data-img" >008</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the top category in particular date and media (Mindshare)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#008A" data-img="data-img" >008A</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to show list article by category id
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#009" data-img="data-img" >009</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the circulation of each category (Reach)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#010" data-img="data-img" >010</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the advertising value (Advalue)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#011" data-img="data-img" >011</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the get the number of input article from top media
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#012" data-img="data-img" >012</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the get the number of input article from top media
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#013" data-img="data-img" >013</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the get article in particular clientset, mediaset, and date. ( Cliptrac )
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#014" data-img="data-img" >014</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to show the detail of the articles
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#015" data-img="data-img" >015</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the today’s news
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#015C" data-img="data-img" >015C</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get count todays news (Bar Dashboard)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#016" data-img="data-img" >016</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the change of the article amount in a time frame (mentiontrac)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#017" data-img="data-img" >017</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to calculate log DPU per company
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#017A" data-img="data-img" >017A</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to calculate log DPU per agent
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#017C" data-img="data-img" >017C</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to calculate log DPU per agent ALL
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#018" data-img="data-img" >018</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get  tone group by date (Tone)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#019" data-img="data-img" >019</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get  tone group by catgory_id (Tone)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#020" data-img="data-img" >020</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the amount of article id group by issue (Issue)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#021" data-img="data-img" >021</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get the searched article (Search)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#022" data-img="data-img" >022</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to Show Summary Per Article (Summary)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#023" data-img="data-img" >023</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to update tone (Tone)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#024" data-img="data-img" >024</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to update summary by client_id and article_id (Summary)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#025" data-img="data-img" >025</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get list saved query (Query)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#025I" data-img="data-img" >025I</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to Insert New Saved Query (Query)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#026" data-img="data-img" >026</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get quotes between 2 speakers (Quotes)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#027" data-img="data-img" >027</a></b>          
								</td>
								<td class="views-field views-field-body">
									Run Query (Query)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#027A" data-img="data-img" >027A</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to show list article in run query by keywords (Query)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#028" data-img="data-img" >028</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get list media where choosen by client id (Setting)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#029" data-img="data-img" >029</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get list sub client / category choosen by client id (Setting)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#030" data-img="data-img" >030</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get list keyword by client id and category (Setting)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#030I" data-img="data-img" >030I</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to insert new keyword (Setting)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#030D" data-img="data-img" >030D</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to delete keyword (Setting)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#033" data-img="data-img" >033</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to show spokeperson by initial (Spokeperson)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#034" data-img="data-img" >034</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to show spokeperson match suggestion (Spokeperson)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#034S" data-img="data-img" >034S</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to show specific spokeperson (Spokeperson)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#035" data-img="data-img" >035</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to insert list spokeperson array data (Spokeperson)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#035S" data-img="data-img" >035S</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to insert specific spokeperson (Spokeperson)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#036" data-img="data-img" >036</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to view unwanted article by client id (Article)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#036I" data-img="data-img" >036I</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to delete unwanted article by client id(Article)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#037" data-img="data-img" >037</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to check EULA status (EULA)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#037I" data-img="data-img" >037I</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to update EULA status (EULA)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#038I" data-img="data-img" >038I</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to insert issue data and keywords (Issue)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#039" data-img="data-img" >039</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to backtrack keyword (Backtrack)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#040" data-img="data-img" >040</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get today trending topic all media (Bar Dashboard)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#041" data-img="data-img" >041</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get today KOL "Key Opinion Leader" (Bar Dashboard)
								</td>
							</tr> 
							<tr class="odd">
								<td class="views-field views-field-title">
									<b><a href="#042" data-img="data-img" >042</a></b>          
								</td>
								<td class="views-field views-field-body">
									Query to get total today quote (Bar Dashboard)
								</td>
							</tr> 
							
							<tr class="even views-row-last">
								<td class="views-field views-field-title">
									<b>...</b>
								</td>
								<td class="views-field views-field-body">
									More ...
								</td>
							  </tr>
						  </tbody>
					</table>
					
					<!-- 001 -->
					<table class="views-table cols-2">						
						<caption class="judul"  >
							<a title="001" id="001" name="001"></a>#<strong>001</strong>
							<p>
							<a href="#105" title="Back to APIs Process" class="back">APIs Process</a>
							Query to get all list of client set for each client</p>
						</caption>
						<caption>
							<strong>Request</strong>
						</caption>
						<thead>
						<tr>
								<th class="views-field views-field-title">
							  Data Parameter        </th>
								  <th class="views-field views-field-body">
							  Data Type        </th>
							  </tr>
						</thead>
						<tbody>
							<tr class="odd views-row-first">
								<td class="views-field views-field-title">
									<code>
										<ol>
											<li>client_id</li>
										</ol>
									</code>        
								</td>
								<td class="views-field views-field-body"> 
									<code>
										<ul>
											<li>ANS-8</li>
										</ul>
									</code>
								</td>
							</tr> 
						  </tbody>
					</table>
					<table class="views-table cols-2">	
						<caption>
							<strong>Result</strong>
						</caption>
						<thead>
						<tr>
								<th class="views-field views-field-title">
							  Data Parameter        </th>
								  <th class="views-field views-field-body">
							  Data Type        </th>
							  </tr>
						</thead>
						<tbody>
							<tr class="odd views-row-first">
								<td class="views-field views-field-title">
									<code> 
										<ol>
											<li>category_set</li>
											<li>descriptionz</li>											
											<span class="repeated">… repeated</span>
										</ol>
									</code>        
								</td> 
								<td class="views-field views-field-body"> 
									<code>
										<ul>
											<li>N-11</li>
											<li>ANS-100</li>
										</ul>
									</code>
								</td>
							</tr> 
						  </tbody>
					</table>
					
				</div>
			</div> 
		</div> 
	</div> 	
</div> 	
</body>
</html>
