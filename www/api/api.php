<?php

error_reporting(0);

include "../config/requirement.php";


	
$result["code"] 	= "05"; 	
$result["message"] 	= "Other Error"; 	
if(!$conn)
{ 
	$result["code"] 	= "06"; 	
	$result["message"] 	= "Error Database [".__LINE__."]".mysql_error(); 
}
else
{
	$time_start = microtime();
	
	$a 		= (array_key_exists('a',$_GET)) ? $_GET['a'] : "" ;
	$d 		= (array_key_exists('data',$_GET)) ? $_GET['data'] : "" ;
	$keys 	= (array_key_exists('keys',$_GET)) ? $_GET['keys'] : "" ;
	$uid	= (array_key_exists('uid',$_GET)) ? $_GET['uid'] : "" ;
	$lolos 	= true;
	
	//checking a parameter
	if($a == "")
	{
		$result["code"] 	= "01"; 	
		$result["message"] 	= "Wrong Parameter";
		$lolos = false;
	}
	
	//checking d parameter
	if($lolos)
	{
		if($d == "")
		{
			$result["code"] 	= "01"; 	
			$result["message"] 	= "Wrong Parameter";
			$lolos = false;
		}
	}
	//checking keys parameter
	if($lolos)
	{
		if($keys == "")
		{
			$result["code"] 	= "01"; 	
			$result["message"] 	= "Wrong Parameter";
			$lolos = false;
		}
		else
		{
			$agent = check_keys($keys);
			if(!$agent)
			{
				$result["code"] 	= "04"; 	
				$result["message"] 	= "Wrong Keys ";
				$lolos = false;
			}
		}
	}
	
	//checking user id parameter
	if($lolos)
	{
		if($uid == "")
		{
			$result["code"] 	= "01"; 	
			$result["message"] 	= "Wrong Parameter";
			$lolos = false;
		}
		else
		{
			$agent = check_uid($agent,$uid);
			if(!$agent)
			{
				$result["code"] 	= "09"; 	
				$result["message"] 	= "Wrong UID ";
				$lolos = false;
			}
		}
	}
	
	//checking action code
	if($lolos)
	{
		if(!check_action_code($a))
		{
			$result["code"] 	= "02"; 	
			$result["message"] 	= "Wrong Action Code";
			$lolos = false;
		}
	}
	
	// checking data
	if($lolos)
	{
		$decode_data 	= base64_decode($d,true);
		if(!$decode_data)
		{
			$result["code"] 	= "03"; 	
			$result["message"] 	= "Wrong Data Parameter";
			$lolos = false;
		}
		else
		{ 
			$array_data		= unserialize($decode_data);
			if(!is_array($array_data))
			{
				$result["code"] 	= "03"; 	
				$result["message"] 	= "Wrong Data Parameter";
				$lolos = false;
			}
		}
	}
	
	if($lolos)
	{
		$lib_file = "../lib/".$a.".php";
		if(file_exists($lib_file))
		{
			include "../lib/".$a.".php";
		}
		else
		{
			$result["code"] 	= "91"; 	
			$result["message"] 	= "System Internal Error";
			$lolos = false;
		}
	}
	$time_end = microtime();
	$time_div = $time_end -  $time_start;

	
	
} 

$out = json_encode($result);
InsertToLog($keys,$uid,$a,$time_div,$d,$result["code"],$out);
mysql_close();
exit($out);
?>
