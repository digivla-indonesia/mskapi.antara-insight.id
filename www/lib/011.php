<?php
// 011
// Query to get the get the number of input article from top media
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id
// $time_frame => 7
// $best => 5
	$lolos = true;
	$arr_parrameters = array( "category_id","media_id","best","time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
		
		if( count($array_data['media_id']) < 1)
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter (media_id) Not Complete";
		} 
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['best']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
		$tblnameA = "mediashare_mmry_".$rand_time."_".$uid;
	// STEP A
	if($lolos)
	{		
		$drop = DropMemory($tblnameA);   
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{ 
			
			$select = 'media_id, COUNT(article_id) as total';
			$group = 'media_id';
			$order = 'total DESC';
			$addQuery = GetArticleGroupCategory($array_data,$tb_category_data,$select,$group,$order); 
			//echo $addQuery;
			$s_create = " CREATE TABLE ".$tblnameA." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		} 
		
	}
	
		$tblnameB = "mediashare_selected_".$rand_time."_".$uid;
	// STEP B
	if($lolos)
	{ 
		$drop = DropMemory($tblnameB);    
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{  
			$best =  $array_data['best'] - 1;
			$s_createB = " CREATE TABLE ".$tblnameB." ENGINE=MEMORY SELECT media_id, total FROM ".$tblnameA."  LIMIT ".$best." ; ";  
			$createB = mysql_query($s_createB); 
			if(!$createB)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."] {".$s_create."} ".mysql_error();
			}
		}  
	}
	// STEP C
	 
	if($lolos)
	{  		
			
		$banyakOther = 0;
		$s_cekCount ="  SELECT  SUM(total) as banyak FROM ".$tblnameA."  WHERE media_id NOT IN (SELECT media_id FROM ".$tblnameB.")  ";
		$q_cekCount = mysql_query($s_cekCount); 
		if($q_cekCount)
		{
			$d_cekCount = mysql_fetch_array($q_cekCount);
			$banyakOther = $d_cekCount['banyak'];
		}
		 
		if($banyakOther > 0)
		{		
			$s_insert = "INSERT INTO ".$tblnameB." "
				." SELECT -1 ,  SUM(total) FROM ".$tblnameA." "
				." WHERE media_id NOT IN (SELECT media_id FROM ".$tblnameB.") " ; 
			$q_insert = mysql_query($s_insert); 
					
			if(!$q_insert)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			} 
		} 
	}
	
	$hasil = array();
	if($lolos)
	{
		$getMmry = GetDataMemory($tblnameB);
			
		if($getMmry[0])
		{
			$hasil = $getMmry[1];
			$lolos = $getMmry[0];
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	}
	
	
	$media = array();
	// STEP D
	if($lolos)
	{ 
		$s_select = GetMediaName($hasil,'media_id');
		//echo $s_select;
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$media = $getMmry[1];
			$lolos = $getMmry[0];
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	}
	//echo "<pre>".var_export($media,true)."</pre>";
	if(count($media) > 0)
	{
		foreach($media as $k => $v)
		{
			$d_media[$v['media_id']] = $v['media_name'];
		}
	}
	
	$theData = array();
	foreach($hasil as $k => $v)
	{ 
		$total = $v['total'];
		$media_name = $d_media[$v['media_id']]; 
		$theData[] = array(
			"media_name" => $media_name,
			"total" => $total, 
		); 
	}
	
	//hapus kembali STEP D
		$allTable = "`".$tblnameA."`,`".$tblnameB."`";
		$dropA = DropMemory($allTable); 
		if(!$dropA)
		{ 	
			$result["notice"] 	= "Error Database [".__LINE__."]".mysql_error();
		}   
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
	} 

	
?>
