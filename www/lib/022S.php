<?php

	include 'TextSummarizer.php'; 
	include 'TextSummarizer2.php'; 


function checkSummaryExist($article_id,$client_id,$content,$title)
{
	global $conn;
	
	$jumdata = 0;
	$res_summary = "";
	$res_keyword = "";
	$addQuery1 		= "SELECT article_id, summary "
							." FROM "._DB_NAME_.".tb_summary_data "
							."	WHERE article_id = ".$article_id."  and client_id = '".$client_id."'  limit 1 "; 
	$getMmry = GetQuery($addQuery1);		
	if($getMmry[0])
	{
		$hasilsummary 		= $getMmry[1];
		$jumdata		 	= $getMmry[2];
		$lolos 				= true;
	} 
	
	if($jumdata > 0)
	{
		// Udah ada di database mari kita ambil saja 
		$res_summary = $hasilsummary[0]['summary'];
	}
	else
	{
		// Belum ada summary mari kita summarize dolo 
		
		 preg_match_all('/(\r\n|\n)/im', $content, $crfeeds, PREG_PATTERN_ORDER);
		$par = count($crfeeds[0]);
		// summarize content here
		if($par > 2)
		{
			$split	 	= explode(chr(13),$content);
			$firstpar 	= $split[0];
			$len 		= strlen($firstpar);
			if($len > 600)
			{
				$TS = CreateTextSummarizerOld($conn, false, $content, $title, false); 
			}
			else
			{
				$TS = CreateTextSummarizerNew($conn, false, $content, $title, false);  
			}
			
		}
		else
		{			
			$TS = CreateTextSummarizerOld($conn, false, $content, $title, false); 
		} 
		
		 
		// end summarize
		
		$res_summary = $TS->textSummary; 
		$res_keyword = $TS->keyWords;
		// var_dump($TS);
		//echo $par."<hr>";
		//echo $res_summary."<hr>";
		
		if($res_summary !== '')
		{		
			$s_insSum = "insert into "._DB_NAME_.".tb_summary_data  SET "
						." `client_id` = '".$client_id."' , "
						." `article_id` = '".$article_id."' , "
						." `summary` = '".mysql_real_escape_string($res_summary)."'   " ;
			$q_insSum = mysql_query($s_insSum);
		}
		
		
	}
	
	$res['summary'] = $res_summary;
	$res['keyword'] = $res_keyword;
	
	return $res;
	
}

	$lolos = true; 
	$arr_parrameters = array("category_id","media_id","time_frame","date_from","date_to","start","limit","type");  
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['start']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['limit']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	} 
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	
	// STEP A 
	if($lolos)
	{  		
		if($lolos)
		{
			$select = 'article_id, tone';
			$group = 'article_id';
			$order = 'datee,tone DESC';
			$addQuery = GetArticleGroupCategory($array_data,$tb_category_data,$select,$group,$order); 
			//echo $addQuery."<hr>";
			$getMmry = GetQuery($addQuery);		
			if($getMmry[0])
			{
				$hasilA 	= $getMmry[1];
				$total_row 	= $getMmry[2];
				$lolos 		= $getMmry[0];
			}
			else
			{
				$result 	= $getMmry[1];
				$total_row 	= $getMmry[2];
				$lolos 		= $getMmry[0];  	
			} 
		}
	} 
	
	if($lolos)
	{  
		if($lolos)
		{
			$myTone = array();
			
			$article_id = "";
			foreach($hasilA as $k => $v)
			{
				$article_id .= "".$v['article_id'].",";
				
				$myTone[$v['article_id']] = $v['tone'];
			}
			$article_id = substr($article_id,0,-1); 			
			//echo $article_id."<hr>";	 ;
			$limit = ( $array_data['limit'] == 0 ) ? 30 : $array_data['limit'] ;
			$start = ( $array_data['start'] == "" ) ? 0 : $array_data['start'] ;
			
			$addQuery 		= "SELECT article_id,media_id ,content, title ,datee, page, mmcol ,file_pdf "
							 ." FROM "._DB_NAME_.".tb_articles_".$array_data['type']." "
							 ."	WHERE article_id IN (".$article_id.") ORDER BY datee DESC LIMIT ".$start.",".$limit.";";
			//echo $addQuery 	;
			$getMmry = GetQuery($addQuery);		
			if($getMmry[0])
			{
				$hasilB 	= $getMmry[1];
				$total_rowB = $getMmry[2];
				$lolos 		= $getMmry[0];
			}
			else
			{
				$result 	= $getMmry[1];
				$total_rowB = $getMmry[2];
				$lolos 		= $getMmry[0];  	
			} 
		}
	}
	//echo "<pre>".var_export($hasilB,true)."</pre>";
	
	/*
	if($lolos)
	{
		$addQuery1 		= "SELECT article_id, summary "
							." FROM tb_summary_data "
							."	WHERE article_id IN (".$article_id.") and client_id = '".$client_id."' ";
		//echo 	$addQuery1."<hr>";	
		$getMmry = GetQuery($addQuery1);		
		if($getMmry[0])
		{
			$hasilsummary 		= $getMmry[1];
			$total_rowsummary 	= $getMmry[2];
			$lolos 				= $getMmry[0];
		}
		else
		{
			$result 		= $getMmry[1]; 
			$lolos 				= $getMmry[0];  	
		} 
	}
	*/
	$myResult = array();
	//echo "<pre>".$addQuery1."-->".var_export($hasilsummary,true)."</pre>";
	$datena = $date_awal = $date_akhir = ""; 
	if($lolos)
	{ 
		$s_select = GetMediaName($hasilB,'media_id'); 
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$media = $getMmry[1];
			$lolos = $getMmry[0];
			
			if(count($media) > 0)
			{
				foreach($media as $k => $v)
				{
					$d_media[$v['media_id']] = $v['media_name'];
				}
			}
			$myResult = array();
			$dateGroup = array();
			$MySummary = array();
			
			/*foreach($hasilsummary as $sk => $sv)
			{
				$article_id = $sv['article_id']; 
				$summary = $sv['summary']; 
				$MySummary[$article_id] = $summary;
			}*/
			
			foreach($hasilB as $k => $v)
			{ //article_id, title, datee, media_id , journalist, content, circulation, mmcol  
				$title 		= $v['title']; 
				$media_id = $v['media_id']; 
				$article_id = $v['article_id']; 
				$datee = $v['datee']; 
				$content = $v['content']; 
				$page = $v['page']; 
				$file_pdf = $v['file_pdf']; 
				$mmcol = $v['mmcol']; 
				$tone = $myTone[$article_id]; //$v['tone'];  
        
        $content = str_replace(array("\r\n", "\r", "\n"), "<br />", $content); 
  	  $content = str_replace(array("\\r\\n", "\\r", "\\n"), "<br />", $content); 
      $content = stripslashes(	$content);
        
				//if(16130956 == $article_id){ echo $content."<hr>"; }
				// summarize content here 
					$TheSummarize	= checkSummaryExist($article_id,$client_id,$content,$title);
					
					$summary 		= $TheSummarize['summary']; 
					$tagskeyword 	= $TheSummarize['keyword'];
				// end summarize
				//if(16130956 == $article_id){ echo $content."<hr>"; }
        $summary = str_replace(array("\r\n", "\r", "\n"), "<br />", $summary); 
  	    $summary = str_replace(array("\\r\\n", "\\r", "\\n"), "<br />", $summary); 
        $summary = stripslashes(	$summary);
				 $content = str_replace("<br>", "", $content); 
				$media_name = $d_media[$media_id]; 
				$content_short = (strlen($content) > 100 ) ? substr($content, 0,100)."..."  : $content;
				
				
				$link_url = ( strpos($file_pdf,"http") !== false ) ? $file_pdf : "" ;
				$file_pdf = ( strpos($file_pdf,"http") !== false ) ? "" : $file_pdf ;
				
				$link_url = str_replace("[Y]",date('Y'),$link_url);
					
				$myResult[] = array( 
					"title" => $title, 
					"article_id" => $article_id,
					"media_id" => $media_id,
					"media_name" => $media_name, 
					"content_short" => $content_short,  
					"content" => $content ,  
					"datee" => $datee,  
					"page" => $page,  
					"file_pdf" => $file_pdf,  
					"link_url" => $link_url,  
					"mmcol" => $mmcol,  
					"tone" => $tone,  
					"summary" => $summary,  
					"keyword" => $tagskeyword,  
				); 
				$dateGroup[strtotime($datee)] = $datee ;
				 //if(16130956 == $article_id){ echo $content."<hr>"; }
				
				unset($TS);
			}
			ksort($dateGroup);
			$soJadi = array_keys($dateGroup);
			$cc = count($soJadi) -1 ;
			//$datena = date("j M Y",$soJadi[0])." and ".date("j M Y",$soJadi[$cc]);
			$date_awal =  $soJadi[0];
			$date_akhir =  $soJadi[$cc];
			
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
		
		
	}
	$hasilD = array(); 
	
	if($lolos)
	{		
		$s_select = " SELECT COUNT(media_id) AS jum_media FROM ( "
						." SELECT DISTINCT(media_id)  "
						." FROM tb_articles_".$array_data['type']." "
						." WHERE article_id IN (".$article_id.") ) A";  
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$hasilD = $getMmry[1][0]['jum_media'];
			$lolos = $getMmry[0];
		}
		else
		{
			$hasilD = $getMmry[1];
			$lolos = $getMmry[0];  	
		} 
	}
	
	
	$theData = array(
		"total_article" => $total_row,
		"total_media" => $hasilD,
		"result" => $myResult,
		"date_first" => $date_awal ,
		"date_last" => $date_akhir ,
	);
	 //echo "<pre>".var_export($myResult,true)."</pre>";
	 
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
	}
	
	

?>
