<?php
/**
 * TextSummarizer
 *
 * @package default
 * @author Yanuar Singgih Saputra  
 */
class TextSummarizer2 {
	//Init Variable
	/* MYSQL connection */
	var $mysqli;
	/* Treat documents as case sensitive */
	var $isCaseSensitive;
	/* Text to summarize */
	var $aText;
	/* Text title */
	var $aTitle;
	/* Print debug information */
	var $debug=false;
	
	//Result Variable
	/* Keywords of the text based on words occurence frequency */	
	var $keyWords    = '';
	/* Summary of the text */
	var $textSummary = '';
	/* List all sentences' ranking */
	var $textRanking = array();
	/* List all words ranking */
	var $wordRanking = array();
	
	var $arrWords = array();
	var $allWords = array();
	var $arrTitle = array();	
	var $textArr  = array();
	var $numWords = array();
	var $lenTitle = 0;
	var $keyMax   = 5;	
	var $sumMax   = 4;
	
	//Utility	
	function ArrayToStr($arr) {
		$arrStr = '';
		
		for ($i = 0; $i < count($arr); $i++)
			$arrStr .= chr($arr[$i]);
		
		return $arrStr;
	}
	
	
	function SelectFromDB($sql, $columnName, $tableName, $criteria) {
		$format = 'SELECT %s FROM %s ';
		$query  = sprintf($format, $columnName, $tableName);
		
		if (strlen($criteria) > 0)
			$query = sprintf('%s WHERE %s', $query, $criteria);
			
			if ($this->debug) {
				printf('<p>Query: %s </p>', $query );
			}
		
		$res = mysql_query($query);
		if($res)
		{			
			$arr_res = mysql_fetch_array($res);
		}
		else
		{
			$arr_res = false;
		}
		return $arr_res;
	}
	
	//Algorithm
	function LSW($s1, $s2) {
		$MaxLen = max(strlen($s1), strlen($s2));
		$LD = levenshtein($s1, $s2);
		$LSW = ($MaxLen - $LD)/$MaxLen;
		
		return $LSW;
	}

	function CreateWordsList($textArr) {
		$aTitle = $this->arrTitle;
		$aWords = $this->arrWords;
		$lWords = $this->allWords;
		$nWords = $this->numWords;
		
		for ($t = 0; $t < count($textArr); $t++) {
			$pattern = '/[.,\[\]{}()"\'\'\*+?\\^$|]/';
			$sentence = preg_replace($pattern, '', $textArr[$t]);
			$sWords = explode(" ", $sentence);
			
			for ($i = 0; $i < count($sWords);$i++) {
				if (!$this->isCaseSensitive)
					$idx = strtolower($sWords[$i]);
				else
					$idx = $sWords[$i];
					
				if (isset($aWords[$t][$idx]))
					$aWords[$t][$idx]++;
				elseif (strlen($idx) > 0)
					$aWords[$t][$idx] = 1;
				
				if (isset($nWords[$t]))
					$nWords[$t]++;
				else
					$nWords[$t] = 1;
				
				if (isset($lWords[$idx]) and strlen($idx) > 0)
					$lWords[$idx]++;
				elseif (strlen($idx) > 0)
					$lWords[$idx] = 1;
			}
		}
		
		// Words in title has heavier weight than normal words
		for ($i = 0; $i < count($aTitle); $i++)
		{
			if(isset($lWords[$aTitle[$i]]))
				$lWords[$aTitle[$i]] += 1.5;
			else
				$lWords[$aTitle[$i]] = 1.5;
		}
		arsort($lWords);
		$this->arrWords = $aWords;
		$this->allWords = $lWords;
		$this->numWords = $nWords;
	}

	function AffWeight($aWord, $sentence_idx) {
		$aWords = $this->arrWords;
		$aTitle = $this->arrTitle;
		$lTitle = $this->lenTitle;
		$iCS    = $this->isCaseSensitive;
		
		if (!$iCS)
			$idx = strtolower($aWord);
		else
			$idx = $aWord;
		
		if (isset($aWords[$sentence_idx][$idx]))
		{
			//Count affinity weight on sentence
			$AW1 = $aWords[$sentence_idx][$idx]/array_sum($aWords[$sentence_idx]);
			
			//Count affinity weight on title (important sentences)
			if(in_array($idx, $aTitle))
				$AW2 = $aWords[$sentence_idx][$idx]/$lTitle;
			else
				$AW2 = 0;
					
			$AW = $AW1 + $AW2;
		}
		else
			$AW = 0;
		
		return $AW;
	}

	function SentenceWeight($sentence_idx) {
		$aText = $this->textArr;
		$aWords = $this->arrWords;
		$sumAW = 0;
		
		//print_r($aText);
		
		$pattern  = '/[.,\[\]{}()"\'\'\*+?\\^$|]/';
		$sentence = preg_replace($pattern, '', $aText[$sentence_idx]);		
		$sWords   = explode(" ", $sentence);
			
		for ($i = 0; $i < count($sWords); $i++)
			$sumAW += $this->AffWeight($sWords[$i], $sentence_idx);
			
		$SW = $sumAW / count($sWords);
		
		return $SW;
	}

	function CompressionHash($sentence) {
		$pattern = '/[.,\[\]{}()"\'\'\*+?\\^$|]/';
		$iCS     = $this->isCaseSensitive;
		
		$sentence = preg_replace($pattern, '', $sentence); 
		$sWords   = explode(" ", $sentence);
		
		for ($i = 0; $i < count($sWords); $i++) {
			$Hw = 0;
			$word = $sWords[$i];
			$word = preg_replace("/[^A-Za-z]/", '', $word);
			for ($j = 0; $j < strlen($word); $j++) {
				if ($iCS)
					$aWord = $word[$j];
				else
					$aWord = strtolower($word[$j]);
				$Hw += ord($aWord)*pow(2, strlen($word)-2);
			}
			$Hw = $Hw % 26;
			$arrHw[$i] = $Hw+65;		
		}
		
		return $this->ArrayToStr($arrHw);
	}

	function VertexWeight($sentence_idx, $compressedText) {
		$VW = 0;
		
		for ($i = 0; $i < count($compressedText); $i++)
		{
			if ($i != $sentence_idx)
				$VW += $this->LSW($compressedText[$i], $compressedText[$sentence_idx]);
		}
		
		return ($VW / count($compressedText));
	}

	function FilterText($text) {	
		$sql = $this->mysqli;
		$bPattern = '/(\x28([^\x28\r\n]+)\x29)/';
		$text     = preg_replace($bPattern, '', $text);
		$pattern  = '/[.,\[\]{}()"\'\'\*+?\\^$|]/';
		$arrText  = explode(" ", preg_replace($pattern, '', $text));
		$idxFound = 0;
		$arrFound = array();
		
		for ($i = 0; $i < count($arrText); $i++) {
			if (!in_array($arrText[$i], $arrFound)) {
				$crit  = sprintf("stop_word LIKE '%s'", $arrText[$i]);
				$found = $this->SelectFromDB($sql, '*', ' `db_msk_produksi`.tb_words_stop ', $crit);
				
				if (count($found) > 0) {
					$arrFound[$idxFound] = sprintf('/\b%s\b/', preg_quote($arrText[$i], '/'));
					$idxFound++;
				}
			}
		}
		
		//Clean text
		$text = preg_replace($arrFound, '', $text);
		$text = preg_replace('!\s+!', ' ', $text);
		
		return $text;
	}
	
	function GetTextTitle($text) {
		$text = preg_replace('/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/', ' ', $text); //Buang semua baris baru
		$re = '/(?<=[.!?]|[.!?][\'"])\s+/';
		$pText = implode(PHP_EOL, preg_split($re, $text, -1, PREG_SPLIT_NO_EMPTY));
		$oTextArr = explode(PHP_EOL, $pText);
		$i = 0;
		if ($this->debug)
			printf('<pre>Title: %s</pre>', var_export($oTextArr, true));
		while (strlen(trim($oTextArr[$i])) < 1)
			$i++;
		
		return $oTextArr[$i];
	}
	
	function GetTextPart($text) {
		$text = preg_replace('/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/', ' ', $text); //Buang semua baris baru
		$re = '/(?<=[.!?]|[.!?][\'"])\s+/';
		$pText = implode(PHP_EOL, preg_split($re, $text, -1, PREG_SPLIT_NO_EMPTY));
		$oTextArr = explode(PHP_EOL, $pText);
		$i = 0;
		while (strlen(trim($oTextArr[$i])) < 1)
			$i++;
		unset($oTextArr[$i]);
		
		return implode(PHP_EOL, $oTextArr);
	}
	
	function ClearQuotes($aText) {
		$quotePattern = '/("[^"\r\n]+)(\.+)([^"\r\n]*")/';
		$replace = '\1 \3';
		$aText = preg_replace($quotePattern, $replace, $aText);
		
		return $aText;
	}
	
	function Run() {
		//Get text title
		//$this->aTitle = $this->GetTextTitle($this->aText);
		
		$aTitle   = $this->aTitle;
		$arrTitle = $this->arrTitle;
		$lenTitle = $this->lenTitle;
		$textArr  = $this->textArr;
		$debug    = $this->debug;
		$pattern  = '/[:.,\[\]{}()"\'\'\*+?\\^$|]/';
		$pattern2 = '';
		
		//$text  = preg_replace('/^[ \t]*$\r?\n/', '', $this->aText);\x28([^\x28\r\n]+)\x29
		$text  = preg_replace('/^[ \t]*[\r\n]+/m', '', $this->aText);
		$pars  = preg_split('/\r?\n/m', $text);
		$quote = "";
		preg_match_all('/".*?".*?\./im', $text, $quotes, PREG_PATTERN_ORDER);
		
		if (strcmp(trim($pars[0]), trim($aTitle)) == 0) {
      for($p = 1; $p < count($pars); $p++) {
             preg_match_all('/\s/i', $pars[$p], $apars, PREG_PATTERN_ORDER);
             $apars = $apars[0];
             if (count($apars) > 9) {
                $fpars = $pars[$p];
                break;
             }
      }      			
			$textSummary = $fpars;
		}
		else {
      for($p = 0; $p < count($pars); $p++) {
             preg_match_all('/\s/i', $pars[$p], $apars, PREG_PATTERN_ORDER);
             $apars = $apars[0];
             if (count($apars) > 9) {
                $fpars = $pars[$p];
                break;
             }
      }
			$textSummary = $fpars;
		}      
			
		for ($i = 0; $i < count($quotes[0]); $i++) {
			if (preg_match("/".preg_quote($quotes[0][$i])."/", $textSummary) == 0) {
				$quote = ". " . trim($quotes[0][$i]);
				break;
			}
		}
		
		$textSummary .= $quote;
				
		//Save result
		$this->textSummary = $textSummary;
		//$this->textRanking = $sentenceRank;
		$this->keyWords    = array();
		
		//Print debug information
		if ($debug) {
			//printf('<p>Array:<br/>%s<br/><br/>%s</p>', var_export($oTextArr, true), var_export($this->textRanking, true));
			printf('<p>Original:<br/>%s</p>', $this->aText);
			printf('<p>Summary:<br/>%s</p>', $this->textSummary);
			//printf('<p>Keywords:<br/>%s</p><hr/>', $this->keyWords);
		}
	}
} // END 

function CreateTextSummarizerNew($mysqli, $isCaseSensitive, $aText, $aTitle, $debug=false) {
	$TS = new TextSummarizer2();
	$TS->mysqli = $mysqli;
	$TS->isCaseSensitive = $isCaseSensitive;
	$TS->aText = $aText;
	$TS->aTitle = $aTitle;
	$TS->debug = $debug;
	$TS->Run();
	
	return $TS;
}
?>
