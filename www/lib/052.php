<?php 
// Query to get the amount of article id group by issue
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id
// $time_frame => 7
	$lolos = true;
	$arr_parrameters = array("category_id","media_id","time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	 	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	$active_ctg_id = array();
	if($lolos)
	{
		$str_category_id = "";
		$category_id = $array_data['category_id'];
		foreach($category_id as $k => $v)
		{
			$str_category_id .= "'".$v."',";
		}	
		$str_category_id = substr($str_category_id,0,-1);
		
		if($str_category_id !== "")
		{ 
			$s_activeCtg = GetActiveIssue($str_category_id); 
			$d_activeCtg = GetQuery($s_activeCtg);		
			if($d_activeCtg[0])
			{
				$active_ctg_id 	= $d_activeCtg[1];
				$total_ctg 	= $d_activeCtg[2];
				$lolos 		= $d_activeCtg[0];
			}
			else
			{
				$result 	= $d_activeCtg[1];
				$total_ctg 	= $d_activeCtg[2];
				$lolos 		= $d_activeCtg[0];  	
			}
		}
		else
		{
			$lolos = false;
			$result["code"] 	= "03"; 	
			$result["message"] 	= "Wrong Data Parameter";
		}

	}  
	$hasil2 = array();
	if($lolos)
	{
		$time_frame = $array_data['time_frame']; 
		$date_from = $array_data['date_from']; 
		$date_to = $array_data['date_to']; 
		$media_date = GetMediaDate($time_frame,$date_from,$date_to);
		
		$str_active_category_id = "";
		foreach($active_ctg_id as $k => $v)
		{ 
			$str_active_category_id .= "'".$v['active_category_id']."',";
		}
		$str_active_category_id = substr($str_active_category_id,0,-1);
		
		$data_media_id = $array_data['media_id'];
		$str_media_id = "";
		foreach($data_media_id as $k => $mdid)
		{ 
			$str_media_id .= "".$mdid.",";
		}
		$str_media_id = substr($str_media_id,0,-1);
		
		$s_select = "SELECT article_id as  article_id_issue  "
		." FROM "._DB_NAME_.".tb_issue_data WHERE category_id IN (".$str_active_category_id.") "
		." ".$media_date." "
		."  AND media_id IN (".$str_media_id.") AND client_id = '".$client_id."'"; 
		$d_select = GetQuery($s_select);		
		if($d_select[0])
		{
			$hasil2		= $d_select[1]; 
			$lolos 		= $d_select[0];
		}
		else
		{
			$result	 	= $d_select[1]; 
			$lolos 		= $d_select[0];  	
		}
		
		
	}
	$hasil =array();
	if($lolos)
	{
		
		$str_article_id = "";
		foreach($hasil2 as $k => $v)
		{ 
			$str_article_id .= "'".$v['article_id_issue']."',";
		}
		$str_article_id = substr($str_article_id,0,-1);
		
		$s_select2 = NewsValueArticle($str_article_id); 
		$d_select2 = GetQuery($s_select2);		
		if($d_select2[0])
		{
			$hasil 		= $d_select2[1]; 
			$lolos 		= $d_select2[0];
		}
		else
		{
			$result	 	= $d_select2[1]; 
			$lolos 		= $d_select2[0];  	
		}
		
	}
	$media = array();
	// STEP D
	if($lolos)
	{ 
		$s_select = GetMediaName($hasil,'media_id'); 
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$media = $getMmry[1];
			$lolos = $getMmry[0];
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	} 
	
	if(count($media) > 0)
	{
		foreach($media as $k => $v)
		{
			$d_media[$v['media_id']] = $v['media_name'];
		}
	} 
	//SELECT title, datee, media_id, page, mmcol, tone, journalist,  
	//  mmcol * rate_bw as news_values, mmcol * rate_bw * 3 as pr_value  
	$theData = array();
	foreach($hasil as $k => $v)
	{ 
		$title 			= $v['title'];
		$datee 			= $v['datee'];
		$page 			= $v['page']; 
		$mmcol 			= $v['mmcol'];
		$tone 			= $v['tone'];
		$journalist 	= $v['journalist'];
		$news_values 	= $v['news_values'];
		$pr_value 		= $v['pr_value'];
		$article_id		= $v['article_id'];
		$media_name 	= $d_media[$v['media_id']]; 
		
		$issue_id = GetIssueByArticleId($article_id,$client_id);
		
		$theData[] = array(
			'title' => $title,
			'media_name' => $media_name,
			'datee' => $datee,
			'page' => $page,
			'mmcol' => $mmcol,
			'tone' => $tone,
			'issue' => $issue_id,
			'journalist' => $journalist,
			'news_values' => $news_values,
			'pr_value' => $pr_value,
		); 
		 
	}
	 
	   
	 
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
	} 

	
?>
