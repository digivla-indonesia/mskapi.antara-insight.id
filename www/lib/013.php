<?php
// 013
// Query to get the get article in particular clientset, mediaset, and date
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id 
// $time_frame => 7
	$lolos = true;
	$arr_parrameters = array( "category_id","media_id", "time_frame","date_from","date_to", "start", "limit");
	
	
		$theData = array();
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	} 
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['start']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	} 
	if($lolos)
	{
		if(!is_numeric($array_data['limit']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	} 
	
	$d_media = array();
	if($lolos)
	{  
		$s_select = GetMediaNameFromParams($array_data); 
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$media = $getMmry[1];
			$lolos = $getMmry[0];
			
			if(count($media) > 0)
			{
				foreach($media as $k => $v)
				{
					$d_media[$v['media_id']] = $v['media_name'];
				}
			}
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	}
	 
	 
	if($lolos)
	{  
		$myResult 		= array();
		$total_rows 	= 0;
		if($lolos)
		{ 
			
			$category_id = "";
			foreach($array_data['category_id'] as $k => $v)
			{
				$category_id .= "'".$v."',";
			}
			$category_id = substr($category_id,0,-1);
			 
			$media_id = "";
			foreach($array_data['media_id'] as $k => $v)
			{
				$media_id .= "'".$v."',";
			}
			$media_id = substr($media_id,0,-1);
			 
			$time_frame =  $array_data['time_frame'];
			$date_from =  $array_data['date_from'];
			$date_to =  $array_data['date_to'];
			
			$queryDatee = GetMediaDate($time_frame,$date_from,$date_to);
			
			$s_select = "  SELECT article_id "
						." FROM ".$tb_category_data." "
						." WHERE category_id IN (".$category_id.") "
						." AND media_id IN (".$media_id.") "
						." ".$queryDatee." GROUP BY article_id "; 
			$getMmry = GetQuery($s_select);
				
			if($getMmry[0])
			{
				$hasilarticle = $getMmry[1];
				$total_rows = $getMmry[2];
				$lolos = $getMmry[0];
			}
			else
			{
				$result = $getMmry[1];
				$total_rows = $getMmry[2];
				$lolos = $getMmry[0];
			} 
		}
		
		$myResult['total_row'] = $total_rows;
		
		if($lolos)
		{			
			
			
			$article_id = "";
			foreach($hasilarticle as $k => $v)
			{
				$article_id .= "'".$v['article_id']."',";
			}
			$article_id = substr($article_id,0,-1);
			$limit = ( $array_data['limit'] == 0 ) ? 30 : $array_data['limit'] ;
			$start = ( $array_data['start'] == "" ) ? 0 : $array_data['start'] ;
			
			$QUERY = "SELECT  article_id, content, datee, title, media_id, journalist,  circulation, mmcol  ,SUBSTRING(file_pdf, 16,4), page , file_pdf "
			." FROM tb_articles	"
			." WHERE  article_id IN (".$article_id.")  " 
			." ORDER BY datee DESC  LIMIT ".$start.",".$limit."; "; 
			$getData = GetQuery($QUERY);
			$hasil = array();
			if($getData[0])
			{
				$hasil = $getData[1];
				$lolos = $getData[0];
			}
			else
			{
				$result = $getData[1];
				$lolos = $getData[0];
			} 
		}
		
		foreach($hasil as $k => $v)
		{  
			 //article_id, title, datee, media_id , journalist, content, circulation, mmcol
			$article_id = $v['article_id'];
			$title = $v['title'];
			$tone = GetToneByArticleID($article_id,$client_id);
			$datee = $v['datee'];
			$media_id = $v['media_id'];
			$journalist = $v['journalist'];
			$content = nl2br($v['content'],false);
			$circulation = $v['circulation'];
			$mmcol = $v['mmcol'];
			$page = $v['page'];
			$file_pdf = $v['file_pdf'];
			$media_name = $d_media[$media_id]; 
			$content = str_replace(array("\r\n", "\r", "\n"), "<br />", $content); 
  	  $content = str_replace(array("\\r\\n", "\\r", "\\n"), "<br />", $content); 
      $content = stripslashes(	$content);
      $content = str_replace("<br>", "", $content); 
			
			$link_url = ( strpos($file_pdf,"http") !== false ) ? $file_pdf : "" ;
			$file_pdf = ( strpos($file_pdf,"http") !== false ) ? "" : $file_pdf ;
			
			$link_url = str_replace("[Y]",date('Y'),$link_url);
			
			$theData[] = array(
				"article_id" => $article_id,
				"title" => $title,
				"datee" => $datee,
				"media_id" => $media_id,
				"media_name" => $media_name,
				"journalist" => $journalist, 
				"content" => $content, 
				"circulation" => $circulation, 
				"mmcol" => $mmcol, 
				"page" => $page,  
				"file_pdf" => $file_pdf,  
				"link_url" => $link_url,  
				"tone" => $tone,  
			); 
		}
		$myResult['result'] = $theData;
		
	} 
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $myResult;
	} 

	
?>
