<?php
// 012
// Query to show the amount of input articles group by category in a range of a date
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id
// $time_frame => 7
	$lolos = true;
	$arr_parrameters = array( "category_id","media_id","best","time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['best']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	 
	if($lolos)
	{ 
			
		if($lolos)
		{		
			$select = 'media_id, COUNT(article_id) total';
			$group = 'media_id';
			$order = 'total DESC';
			$best =  $array_data['best'] - 1;
			$addQuery = GetArticleGroupCategory($array_data,$tb_category_data,$select,$group,$order);	
			$getMmry = GetQuery($addQuery." LIMIT ".$best."; ");
			
			if($getMmry[0])
			{
				$hasil = $getMmry[1];
				$lolos = $getMmry[0];
			}
			else
			{
				$result = $getMmry[1];
				$lolos = $getMmry[0];
			} 
		}
		
		
	}
// STEP D
	if($lolos)
	{ 
		$s_select = GetMediaName($hasil,'media_id');
		//echo $s_select;
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$media = $getMmry[1];
			$lolos = $getMmry[0];
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	}
	//echo "<pre>".var_export($media,true)."</pre>";
	if(count($media) > 0)
	{
		foreach($media as $k => $v)
		{
			$d_media[$v['media_id']] = $v['media_name'];
		}
	}
	
	$theData = array();
	foreach($hasil as $k => $v)
	{ 
		$total = $v['total'];
		$media_name = $d_media[$v['media_id']]; 
		$theData[] = array(
			"media_name" => $media_name,
			"total" => $total, 
		); 
	}
	 
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
	} 

	
?>
