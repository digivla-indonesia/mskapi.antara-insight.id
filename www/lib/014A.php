<?php
// 014 Query to show the detail of the articles 
	$lolos = true;
	$arr_parrameters = array( "article_id");
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	 
	if($lolos)
	{
		$article_id =  $array_data['article_id'];
		  
		if($lolos)
		{
 
			$s_select = "SELECT article_id, title, datee, media_id , journalist, content, circulation, mmcol ,page,file_pdf "
						." FROM tb_articles WHERE article_id IN (".$article_id.") ";
			// echo $s_select;
			$getMmry = GetQuery($s_select);
			
			if($getMmry[0])
			{
				$hasil = $getMmry[1];
				$lolos = $getMmry[0];
			}
			else
			{
				$result = $getMmry[1];
				$lolos = $getMmry[0];
			} 
		}
		
		if($lolos)
		{ 
			$s_select = GetMediaName($hasil,'media_id'); 
			$getMmry = GetQuery($s_select);
			
			if($getMmry[0])
			{
				$media = $getMmry[1];
				$lolos = $getMmry[0];
			}
			else
			{
				$result = $getMmry[1];
				$lolos = $getMmry[0];
			} 
		}
		//echo "<pre>".var_export($media,true)."</pre>";
		if(count($media) > 0)
		{
			foreach($media as $k => $v)
			{
				$d_media[$v['media_id']] = $v['media_name'];
			}
		}
		
		$theData = array();
		foreach($hasil as $k => $v)
		{ //article_id, title, datee, media_id , journalist, content, circulation, mmcol
			$article_id = $v['article_id'];
			$title = $v['title'];
			$tone = GetToneByArticleID($article_id,$client_id);
			$TheSummarize = getSummary($article_id,$client_id);
			$summary 		= $TheSummarize['summary']; 
			$datee = $v['datee'];
			$media_id = $v['media_id'];
			$journalist = $v['journalist'];
			$content = $v['content'];
			$circulation = $v['circulation'];
			$mmcol = $v['mmcol'];
			$page = $v['page'];
			$file_pdf = $v['file_pdf'];
			$media_name = $d_media[$media_id]; 
			$link_url = ( strpos($file_pdf,"http") !== false ) ? $file_pdf : "" ;
			$file_pdf = ( strpos($file_pdf,"http") !== false ) ? "" : $file_pdf ;
			$link_url = str_replace("[Y]",date('Y'),$link_url);
			$theData[] = array(
				"article_id" => $article_id,
				"title" => $title,
				"datee" => $datee,
				"media_id" => $media_id,
				"media_name" => $media_name,
				"journalist" => $journalist, 
				"content" => $content, 
				"circulation" => $circulation, 
				"mmcol" => $mmcol, 
				"page" => $page,  
				"file_pdf" => $file_pdf,  
				"link_url" => $link_url,  
				"tone" => $tone,  
				"summary" => $summary,  
			); 
		}
		
	}
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
	}
	

function getSummary($article_id,$client_id)
{
	global $conn;
	
	$jumdata = 0;
	$res_summary = ""; 
	$addQuery1 		= "SELECT article_id, summary "
							." FROM "._DB_NAME_.".tb_summary_data "
							."	WHERE article_id = ".$article_id."  and client_id = '".$client_id."'  limit 1 "; 
	$getMmry = GetQuery($addQuery1);		
	if($getMmry[0])
	{
		$hasilsummary 		= $getMmry[1];
		$jumdata		 	= $getMmry[2];
		$lolos 				= true;
	} 
	
	if($jumdata > 0)
	{
		// Udah ada di database mari kita ambil saja 
		$res_summary = $hasilsummary[0]['summary'];
	} 
	
	$res['summary'] = $res_summary; 
	
	return $res;
	
}
?>
