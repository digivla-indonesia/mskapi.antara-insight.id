<?php
// 018
// Query to get tone group by category_id
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id
// $time_frame => 7
	$lolos = true;
	$arr_parrameters = array("category_id","media_id","time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
		
		if( count($array_data['media_id']) < 1)
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter (media_id) Not Complete";
		} 
	}
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	$tblname = "tone_mmry_".$rand_time."_".$uid;
	if($lolos)
	{
		$drop = DropMemory($tblname);   // mysql_query("DROP TABLE IF EXISTS tb_category_mmry_".$rand_time.";");
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{ 
			$time_frame =  $array_data['time_frame']; 
			$select = 'datee, tone, COUNT(article_id) as jum_tone';
			$group = 'datee, tone';
			$order = 'datee ASC'; 
			// JIKA PAST YEAR
			if($time_frame > 350)
			{
				$select = 'LEFT(datee,7) AS datee, tone, COUNT(article_id) as jum_tone';
				$group = 'YEAR(datee), MONTH(datee), tone';
			} 
			$addQuery = GetArticleGroupCategory($array_data,$tb_category_data,$select,$group,$order); 
			$s_create = " CREATE TABLE ".$tblname." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}		 
		
	}
	$hasil = array();
	if($lolos)
	{
		/*$s_storeproc = " CALL spp_xtab('tone', 'tone', "
		." 'FROM ".$tblname." limit 60', 'jum_tone', 'datee', "
		." 'FROM ".$tblname." group by  datee'); ";*/
		$s_storeproc = " CALL spp_xtab('tone', 'tone', "
		." 'FROM ".$tblname."  ', 'jum_tone', 'datee', "
		." 'FROM ".$tblname." group by  datee'); ";
		$q_storeproc = mysql_query($s_storeproc); 
	  
		if(!$q_storeproc)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		else
		{
			$r_query = mysql_num_rows($q_storeproc);
			if($r_query > 0)
			{
				while($d_query = mysql_fetch_assoc($q_storeproc))
				{
					$hasil[] = $d_query;
				}
			}
			else
			{
				$how = false;
				$result["code"] 	= "08"; 	
				$result["message"] 	= "No Data Display" ;
			}
			mysql_free_result($q_storeproc);
		} 
		
		
	} 
	//hapus kembali 
	mysql_close();
	$conn2 = connectToDB(_DB_HOST_,_DB_USER_,_DB_PASS_,_DB_NAME_);
	$drop = DropMemory($tblname); 
	if(!$drop)
	{ 	
		$result["notice"] 	= "Error Database [".__LINE__."]".mysql_error();
	}
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $hasil;
	} 

	
?>
