<?php 
	$lolos = true;  
	$arr_parrameters = array("client_id");  
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	} 
		
	if($lolos)
	{
		if(empty($array_data['client_id']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	} 
	$hasil_choosen 	= array();
	if($lolos)
	{
		$par_client_id 	= $array_data['client_id'];		 
		$par_media_id 	= $array_data['media_id'];
		
		$queryGetMedia = "SELECT media_id FROM  "._DB_NAME_ADM_.".tb_media_client WHERE client_id = '".$par_client_id."' ";
		$GetMedia = GetQuery($queryGetMedia);		
		if($GetMedia[0])
		{  
			$hasil_choosen = $GetMedia[1]; 
		}
		else
		{
			$hasil_choosen 	= array();  
		}  
	}
	$theData = array();
	
	if($lolos)
	{			    
		$addQuery = "SELECT a.media_id, a.media_name, b.media_type FROM "._DB_NAME_.".tb_media a INNER JOIN 
		"._DB_NAME_.".tb_media_type b ON a.media_type_id = b.media_type_id
		WHERE a.media_id >= 0  and a.statuse = 'A' ORDER BY b.media_type ASC  "; 
		$getMmry = GetQuery($addQuery);
		
		if($getMmry[0])
		{
			$hasil 		= $getMmry[1];
			$total_row	= $getMmry[2];
			$lolos 		= $getMmry[0];
		}
		else
		{
			$result 	= $getMmry[1];
			$total_row 	= $getMmry[2];
			$lolos 		= $getMmry[0];
		} 
		
		$choosenData = array();
		foreach($hasil_choosen as $kk => $kv)
		{
			$Media_choosen = $kv['media_id'];
			$choosenData[$Media_choosen] = 1;
		}
		
		foreach($hasil as $k => $v)
		{
			
			$media_id_ = $v['media_id']; 
			$media_name_ = $v['media_name']; 
			$media_type = $v['media_type']; 
			$choosenKah = (array_key_exists($media_id_,$choosenData)) ? $choosenData[$media_id_] : 0;
			 
			$theData[$media_type][] = array(
				"media_id" => $media_id_, 
				"media_name" => $media_name_, 
				"choosen" => $choosenKah,
			);
		}
	}  
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
		$result["total_row"]= $total_row;
	}
	

?>
