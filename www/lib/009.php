<?php
// 009
// Query to get the circulation of each category
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id
// $time_frame => 7
// $best => 5
	$lolos = true;
	$arr_parrameters = array( "category_id","media_id","best","time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
   if( count($array_data['media_id']) < 1)
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter (media_id) Not Complete";
		} 
	}
	
	
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['best']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	// STEP A
	if($lolos)
	{
		 
		
		$tblnameA = "reach_mmry_".$rand_time."_".$uid;
		$drop = DropMemory($tblnameA);   
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{ 
			$select = 'category_id,  SUM(circulation) sum_reach';
			$group = 'category_id';
			$order = 'sum_reach DESC';
			$addQuery = GetArticleGroupCategory($array_data,$tb_category_data,$select,$group,$order);
			$s_create = " CREATE TABLE ".$tblnameA." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		} 
		
	}
	
	// STEP B
	if($lolos)
	{ 
		$tblnameB = "reach_best_".$rand_time."_".$uid;
		$drop = DropMemory($tblnameB);    
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{  
			$best =  $array_data['best'] - 1;
			$s_createB = " CREATE TABLE ".$tblnameB." ENGINE=MEMORY SELECT * FROM ".$tblnameA."  LIMIT ".$best." ; ";  
			$createB = mysql_query($s_createB); 
			if(!$createB)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."] {".$s_create."} ".mysql_error();
			}
		}  
	}
	// STEP C
	if($lolos)
	{
		$tblnameC = "reach_all_mmry_".$rand_time."_".$uid;
		$drop = DropMemory($tblnameC);   
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{ 
			$select = 'category_id,  datee, SUM(circulation) sum_reach';
			$group = 'category_id, datee';
			$order = 'category_id ASC';
			$addQuery = GetArticleGroupCategory($array_data,$tb_category_data,$select,$group,$order);
			$s_create = " CREATE TABLE ".$tblnameC." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		} 
		
	}  
	// STEP D
	if($lolos)
	{
		$tblnameD = "reach_pivot_mmry_".$rand_time."_".$uid;
		$drop = DropMemory($tblnameD);   
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{ 
			$select = 'category_id,  datee, SUM(circulation) sum_reach';
			$group = 'category_id, datee';
			$order = 'category_id ASC';
			$__array_data = $array_data;
			$__array_data['category_id'] = "SELECT category_id FROM ".$tblnameB." ";
			$addQuery = GetArticleGroupCategory($array_data,$tb_category_data,$select,$group,$order);
			$s_create = " CREATE TABLE ".$tblnameD." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		} 
		
	}  
	
	// STEP D
	if($lolos)
	{   
		$s_insert = "INSERT INTO ".$tblnameD." "
			." SELECT 'Others',  datee, SUM(sum_reach) FROM ".$tblnameC." "
			." WHERE category_id NOT IN (SELECT category_id FROM ".$tblnameB.") GROUP BY datee" ; 
		$q_insert = mysql_query($s_insert); 
				
		if(!$q_insert)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		} 
	}
	//STEP E
	if($lolos)
	{
		$s_storeproc = " CALL spp_xtab('category_id', 'category_id', "
		." 'FROM ".$tblnameD." limit 60', 'sum_reach', 'datee', "
		." 'FROM ".$tblnameD." group by  datee'); ";
		$q_storeproc = mysql_query($s_storeproc); 
	  
		if(!$q_storeproc)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		else
		{
			$r_query = mysql_num_rows($q_storeproc);
			if($r_query > 0)
			{
				while($d_query = mysql_fetch_assoc($q_storeproc))
				{
					$hasil[] = $d_query;
				}
			}
			else
			{
				$how = false;
				$result["code"] 	= "08"; 	
				$result["message"] 	= "No Data Display" ;
			}
			mysql_free_result($q_storeproc);
		} 
		
		
	}
	//hapus kembali STEP D
		mysql_close();
		$conn2 = connectToDB(_DB_HOST_,_DB_USER_,_DB_PASS_,_DB_NAME_);
		$allTable = "`".$tblnameA."`,`".$tblnameB."`,`".$tblnameC."`,`".$tblnameD."`";
		$dropA = DropMemory($allTable); 
		if(!$dropA)
		{ 	
			$result["notice"] 	= "Error Database [".__LINE__."]".mysql_error();
		}  
		
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $hasil;
	} 

	
?>
