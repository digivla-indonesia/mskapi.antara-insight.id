<?php
// 020
// Query to get the amount of article id group by issue
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id
// $time_frame => 7
	$lolos = true;
	$arr_parrameters = array("category_id","media_id","best","time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['best']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	$active_ctg_id = array();
	if($lolos)
	{
		$str_category_id = "";
		$category_id = $array_data['category_id'];
		foreach($category_id as $k => $v)
		{
			$str_category_id .= "'".$v."',";
		}	
		$str_category_id = substr($str_category_id,0,-1);
		
		if($str_category_id !== "")
		{
		
			//$s_activeCtg = "SELECT client_id, category_id AS 'active_category_id', issue_id FROM "._DB_NAME_ADM_.".tb_issue_keyword "
			//	." WHERE statuse = 'A' AND category_id IN (".$str_category_id.") group by category_id ";
			//echo 	$s_activeCtg ;
			$s_activeCtg = GetActiveIssue($str_category_id);
			$d_activeCtg = GetQuery($s_activeCtg);		
			if($d_activeCtg[0])
			{
				$active_ctg_id 	= $d_activeCtg[1];
				$total_ctg 	= $d_activeCtg[2];
				$lolos 		= $d_activeCtg[0];
			}
			else
			{
				$result 	= $d_activeCtg[1];
				$total_ctg 	= $d_activeCtg[2];
				$lolos 		= $d_activeCtg[0];  	
			}
		}
		else
		{
			$lolos = false;
			$result["code"] 	= "03"; 	
			$result["message"] 	= "Wrong Data Parameter";
		}

	}
	
	$tblnameA = "all_issue_".$rand_time."_".$uid;
	if($lolos)
	{
		$active_category_id = array();
		foreach($active_ctg_id as $k => $v)
		{
			$active_category_id[] = $v['active_category_id']; 
		}
		//echo "<pre>".var_export($active_category_id,true)."</pre>";
		$drop = DropMemory($tblnameA);  
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{  
			$paramsIssue = $array_data;
			$paramsIssue['category_id'] = $active_category_id;
			
			$addQuery = GetArticleGroupIssue($paramsIssue,$client_id); 
			$s_create = " CREATE TABLE ".$tblnameA." ENGINE=MEMORY ".$addQuery." "; 
			//echo 	$s_create ;
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}		 
		
	}
	 
	if($lolos)
	{ 	  
		if($lolos)
		{
			$best = ($array_data['best'] < 5 ) ? 5 : $array_data['best'];
			$best = $best - 1;
			$addQuery = "SELECT issue_id FROM ".$tblnameA." LIMIT ".$best;
			$getMmry = GetQuery($addQuery);
			
			if($getMmry[0])
			{
				$top_cat = $getMmry[1];
				$lolos = $getMmry[0];
			}
			else
			{
				$result = $getMmry[1];
				$lolos = $getMmry[0];  	
			} 
		}
		
	}
	
	$tblnameC = "issuedate_all_".$rand_time."_".$uid;
	if($lolos)
	{
		$drop = DropMemory($tblnameC);   
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{  
			$time_frame =  $array_data['time_frame']; 
			$select = 'issue_id,  datee, COUNT(article_id) as jum_issue';
			$group = 'issue_id, datee';
			$order = 'issue_id ASC';  
			// JIKA PAST YEAR
			if($time_frame > 350)
			{
				$sekarang = date("Y-m-d");
				$select = "issue_id, LEFT('".$sekarang."',7) AS datee, COUNT(article_id) as jum_issue";
				$group = 'issue_id, YEAR(datee), MONTH(datee)';
			} 
			
			$addQuery = GetArticleGroupIssue($paramsIssue,$client_id,$select,$group,$order); 
			$s_create = " CREATE TABLE ".$tblnameC." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}		 
		
	}
	
	$tblnameD = "issue_pivot_".$rand_time."_".$uid;
	if($lolos)
	{
		$drop = DropMemory($tblnameD);   
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{   
		 
			$issue_id = "";
			foreach($top_cat as $k => $v)
			{
				$issue_id .= "'".$v['issue_id']."',";
			}
			$issue_id = substr($issue_id,0,-1); 
			//echo $category_id;
			$addQuery = "SELECT issue_id,  datee,  jum_issue "
						." FROM ".$tblnameC." "
						." WHERE issue_id  IN (".$issue_id.") " 
						." ORDER by datee ASC; "; 
			$s_create = " CREATE TABLE ".$tblnameD." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}		 
		
	} 
	
	if($lolos)
	{  
		$s_insert = "INSERT INTO ".$tblnameD." "
			." SELECT 'Others',  datee, SUM(jum_issue) as total FROM ".$tblnameC." "
			." WHERE issue_id NOT IN (".$issue_id.") GROUP BY datee ORDER by datee ASC" ; 
		$q_insert = mysql_query($s_insert); 
				
		if(!$q_insert)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		} 
	}
	
	
	if($lolos)
	{ 
		
		$s_storeproc = " CALL spp_xtab('datee', 'datee', "
		." 'FROM ".$tblnameD." limit 60', 'jum_issue', 'issue_id', "
		." 'FROM ".$tblnameD." group by  issue_id'); ";
		$q_storeproc = mysql_query($s_storeproc); 
	  
		if(!$q_storeproc)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		else
		{
			$r_query = mysql_num_rows($q_storeproc);
			if($r_query > 0)
			{
				while($d_query = mysql_fetch_assoc($q_storeproc))
				{
					$hasil[] = $d_query;
				}
			}
			else
			{
				$how = false;
				$result["code"] 	= "08"; 	
				$result["message"] 	= "No Data Display" ;
			}
			mysql_free_result($q_storeproc);
		} 
		
		
	}
	//hapus kembali 
	mysql_close();
	$conn2 = connectToDB(_DB_HOST_,_DB_USER_,_DB_PASS_,_DB_NAME_);
	$allTable = "`".$tblnameA."`,`".$tblnameC."`,`".$tblnameD."`";
	$drop = DropMemory($allTable); 
	if(!$drop)
	{ 	
		$result["notice"] 	= "Error Database [".__LINE__."]".mysql_error();
	}
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $hasil;
	} 

	
?>
