<?php
// 015 Query to get the today’s news  
	$lolos = true; 
	$arr_parrameters = array( "start", "limit" ); 
	$theData = array();
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['start']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	} 
	if($lolos)
	{
		if(!is_numeric($array_data['limit']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	} 
	 
	if($lolos)
	{			
		$limit = ( $array_data['limit'] == 0 ) ? 30 : $array_data['limit'] ;
		$start = ( $array_data['start'] == "" ) ? 0 : $array_data['start'] ;
		$sekarang = date("Y-m-d");
		$s_select = "SELECT article_id, title, datee, media_id , journalist, content, circulation, mmcol ,page,file_pdf"
					." FROM tb_articles "
					." WHERE datee = '".$sekarang."' "
					." ORDER BY title  LIMIT ".$start.",".$limit.";	";
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$hasil = $getMmry[1];
			$lolos = $getMmry[0];
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	}
	
	if($lolos)
	{ 
		$s_select = GetMediaName($hasil,'media_id'); 
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$media = $getMmry[1];
			$lolos = $getMmry[0];
			
			if(count($media) > 0)
			{
				foreach($media as $k => $v)
				{
					$d_media[$v['media_id']] = $v['media_name'];
				}
			}
			
			$theData = array();
			foreach($hasil as $k => $v)
			{ //article_id, title, datee, media_id , journalist, content, circulation, mmcol
				$article_id = $v['article_id'];
				$title = $v['title'];
				$datee = $v['datee'];
				$media_id = $v['media_id'];
				$journalist = $v['journalist'];
				$content = $v['content'];
				$circulation = $v['circulation'];
				$mmcol = $v['mmcol'];
				$page = $v['page'];
				$file_pdf = $v['file_pdf'];
				$media_name = $d_media[$media_id]; 
				
				$file_pdf = ( strpos($file_pdf,"http") !== false ) ? "" : $file_pdf ;
				$content = str_replace(array("\r\n", "\r", "\n"), "<br />", $content); 
  	  $content = str_replace(array("\\r\\n", "\\r", "\\n"), "<br />", $content); 
      $content = stripslashes(	$content);
      $content = str_replace("<br>", "", $content); 
				$theData[] = array(
					"article_id" => $article_id,
					"title" => $title,
					"datee" => $datee,
					"media_id" => $media_id,
					"media_name" => $media_name,
					"journalist" => $journalist, 
					"content" => $content, 
					"circulation" => $circulation, 
					"mmcol" => $mmcol, 
					"page" => $page, 
					"file_pdf" => $file_pdf, 
				); 
			}
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	} 
	
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
	} 
	

?>
