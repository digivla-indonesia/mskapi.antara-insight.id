<?php
// function 005 Query to get Current and past news (3 days)
// Query to show the amount of input articles group by category in a range of a date
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id 
	$lolos = true;
	$arr_parrameters = array("media_id","time_frame","date_from","date_to",);
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	} 
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	// STEP A
	
	
	
	if($lolos)
	{  
		
		$media_id = "";
		foreach($array_data['media_id'] as $k => $v)
		{
			$media_id .= "".$v.",";
		}
		$media_id = substr($media_id,0,-1);
		
		 
		
		 if($media_id == "")
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter (media_id) Not Complete";
		} 
		 
	} 
	
	
	if($lolos)
	{			
		//$s_topkol = GetTopTenKOL($datee,$media_id);
		$time_frame =  $array_data['time_frame']; 
		$date_from	=  $array_data['date_from']; 
		$date_to	=  $array_data['date_to']; 
		$s_topkol = GetTopTenKOL($media_id,$time_frame,$date_from,$date_to);
		$getMmry  = GetQuery($s_topkol);
		
		if($getMmry[0])
		{
			$hasil = $getMmry[1];
			$lolos = $getMmry[0];
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	}
	
	if($lolos)
	{
		$Thedata = array();
		foreach($hasil as $row)
		{
			$Thedata['spokeperson'][] = $row['spokeperson'];
			$Thedata['total'][] = $row['total'];
		}
		
		
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $hasil;
		$result["result"] 	= $Thedata;
	} 

	
?>
