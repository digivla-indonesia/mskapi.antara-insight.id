<?php
// 015 Query to get the today’s news  
	$lolos = true; 
	$arr_parrameters = array("keywords","media_id","time_frame","date_from","date_to","start","limit" );  
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['start']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['limit']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	} 
	 
	if($lolos)
	{
		if(!is_array($array_data['keywords']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	
	// STEP A 
	if($lolos)
	{  		
		if($lolos)
		{
			$date_from 		= $array_data['date_from'];
			$date_to 		= $array_data['date_to'];
			$time_frame 	= $array_data['time_frame'];
			$keywords 		= $array_data['keywords'];
			$limit = ( $array_data['limit'] == 0 ) ? 30 : $array_data['limit'] ;
			$start = ( $array_data['start'] == "" ) ? 0 : $array_data['start'] ; 
			
			$q_media_id = "";
			foreach($array_data['media_id'] as $k => $v)
			{
				$q_media_id .= "".$v.",";
			}
			$q_media_id = substr($q_media_id,0,-1);
			 
			$mode =  'extended';
			$mediadate 		= GetMediaDate($time_frame,$date_from,$date_to,'tgl');
	
			$str_keyword = "";
			foreach($keywords as $ii => $textkeyword)
			{
				$cekExcludeWord 	= GetWordExclude($textkeyword);
				if( $cekExcludeWord )
				{
					//do nothing 
				} 
				else
				{
					$str_keyword .= $textkeyword."|";
				}
				
			}
			
			$str_keyword = substr($str_keyword,0,-1); 
			
			$str_keyword = str_replace("'",'"',$str_keyword);
			
			$addQuery 		= "SELECT id as article_id , media_id  "
							." FROM `"._DB_NAME_."`.tb_cari "
							." WHERE (QUERY ='".$str_keyword.";limit=10000000;mode=".$mode.";sort=attr_desc:tgl') "
							." ".$mediadate." "
							." AND media_id IN (".$q_media_id.") ; "; 
			$getMmry = GetQuery($addQuery);		
			if($getMmry[0])
			{ 
				$total_row 	= $getMmry[2]; 
				$hasilA 	= $getMmry[1];
				$lolos 		= $getMmry[0];
			}
			else
			{ 
				$total_row 	= $getMmry[2];
				$result 	= $getMmry[1]; 
				$lolos 		= $getMmry[0]; 	
			} 
			  
		}
	} 
	$jumlah_media = 0;
	if($lolos)
	{  
		if($lolos)
		{
			$MediaAll = array();
			$article_id = "";
			foreach($hasilA as $k => $v)
			{
				$article_id .= "".$v['article_id'].",";
				$MediaAll[] =$v['media_id'];  
			}
			$article_id = substr($article_id,0,-1); 			
			$MediaAll = array_unique($MediaAll); 
			$jumlah_media = count($MediaAll);
			
			$addQuery 		= "SELECT article_id,media_id ,content, title ,datee ,file_pdf "
							 ." FROM "._DB_NAME_.".tb_articles "
							 ."	WHERE article_id IN (".$article_id.") " 
							 ." ORDER BY article_id DESC LIMIT ".$start.",".$limit."; ";
						
			$getMmry = GetQuery($addQuery);		
			if($getMmry[0])
			{
				$hasilB 	= $getMmry[1];
				$total_rowB = $getMmry[2];
				$lolos 		= $getMmry[0];
			}
			else
			{
				$result 	= $getMmry[1];
				$total_rowB = $getMmry[2];
				$lolos 		= $getMmry[0];  	
			} 
		}
	}
	$myResult = array();
	$datena   = $date_awal = $date_akhir = ""; 
	if($lolos)
	{ 
		$s_select = GetMediaName($hasilB,'media_id'); 
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$media = $getMmry[1];
			$lolos = $getMmry[0];
			
			if(count($media) > 0)
			{
				foreach($media as $k => $v)
				{
					$d_media[$v['media_id']] = $v['media_name'];
				}
			}
			
			$dateGroup = array(); 
			foreach($hasilB as $k => $v)
			{ //article_id, title, datee, media_id , journalist, content, circulation, mmcol 
				$title = $v['title']; 
				$media_id = $v['media_id']; 
				$article_id = $v['article_id']; 
				$datee = $v['datee']; 
				$file_pdf = $v['file_pdf']; 
				$content = $v['content']; 
				$media_name = $d_media[$media_id]; 
				$content_short = (strlen($content) > 100 ) ? substr($content, 0,100)."..."  : $content;
				
				$file_pdf = ( strpos($file_pdf,"http") !== false ) ? "" : $file_pdf ;
				
				$myResult[] = array( 
					"title" => $title, 
					"article_id" => $article_id,
					"media_id" => $media_id,
					"media_name" => $media_name, 
					"content_short" => $content_short,  
					"content" => $content,  
					"datee" => $datee,  
					"file_pdf" => $file_pdf,  
				); 
				$dateGroup[strtotime($datee)] = $datee ; 
			}
			ksort($dateGroup);
			$soJadi = array_keys($dateGroup);
			$cc = count($soJadi) -1 ;
			//$datena = date("j M Y",$soJadi[0])." and ".date("j M Y",$soJadi[$cc]);
			$date_awal =  $soJadi[0];
			$date_akhir =  $soJadi[$cc];
			
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
		
		
	}
	  	
	//krsort($myResult);
	$theData = array(
		"total_article" => $total_row,
		"total_media" => $jumlah_media,
		"result" => $myResult,
		"date_first" => $date_awal ,
		"date_last" => $date_akhir , 
	);
	
	 
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
	}
	
	

?>
