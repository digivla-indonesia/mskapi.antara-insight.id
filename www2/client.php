<?php
// example using APIS
function get_content_post($url,$data ) { 
$ch = curl_init();
curl_setopt ($ch, CURLOPT_URL, $url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt ($ch, CURLOPT_TIMEOUT, 49);
ob_start();
curl_exec ($ch);
curl_close ($ch);
$string = ob_get_contents();
ob_end_clean();
return $string;
}
// define the url APIS 
  
  $url = "http://192.168.10.119/mskapi/api.php?";   

// define ALL data parameters 
 $data = array (
  'category_id' => 
  array (
    0 => 'Bank BCA',
    1 => 'Bank BNI',
    2 => 'Bank BRI',
    3 => 'Bank CIMB Niaga',
    4 => 'Bank Mandiri',
  ),
  'media_id' => 
  array (
    0 => '1',
    1 => '2',
    2 => '4',
    3 => '5',
    4 => '6',
    5 => '7',
    6 => '8',
    7 => '10',
    8 => '11',
    9 => '19',
    10 => '23',
    11 => '24',
    12 => '43',
    13 => '96',
    14 => '114',
    15 => '269',
    16 => '382',
    17 => '466',
    18 => '511',
    19 => '581',
    20 => '595',
  ),
  'best' => '20',
  'time_frame' => '7',
  'date_from' => '',
  'date_to' => '',
)
 ;
$serialize  	= serialize($data); // serialize array of data 
$encode_data 	= base64_encode($serialize); //encoding to base64

// build the parameters
$a = ( array_key_exists("a",$_GET )) ? $_GET["a"] : "001" ;
$c = ( array_key_exists("c",$_GET )) ? $_GET["c"] : "inter" ;
$k = ( array_key_exists("k",$_GET )) ? $_GET["k"] : "335f9512fad532f871ba1c242a268961" ;
$u = ( array_key_exists("u",$_GET )) ? $_GET["u"] : "000000001" ;
$params = array(
	"a" => $a,
	"keys" => $k,
	"uid" => $u,
	"data" => $encode_data,
	"client_id" => $c ,
);
$send_params = http_build_query($params);

// sending APIs data
$hasil = file_get_contents($url."".$send_params);
//$hasil = get_content_post($url,$params); 

// result here
//echo "<h2>HASIL JSON FORMAT : </h2><BR>";
echo $hasil;  
$decode = json_decode($hasil,1);
//echo "<h2>Setelah di decode menjadi array assoc : </h2><BR>";
//echo "<pre>".var_export($decode,true)."</pre>"; 
//show_source(__FILE__);


?>
