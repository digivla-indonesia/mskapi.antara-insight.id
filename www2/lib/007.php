<?php
// 007
// Query to show the number of article id in particular category and media
// $category_id => '@category_id1', '@category_id2'
// $media_id => '@media_id', '@media_id2'
// $best => @best 
// $time_frame => 7
	$lolos = true;
	$arr_parrameters = array( "category_id","media_id","best","time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
   if( count($array_data['media_id']) < 1)
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter (media_id) Not Complete";
		} 
	}
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['best']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	$tblnameA = "mmry_".$rand_time."_".$uid;
	$tblnameB = "chart_pivot_".$rand_time."_".$uid;
	$tblnameC = "chart_all_".$rand_time."_".$uid;
	$tblnameD = "chart_".$rand_time."_".$uid;
	// STEP A
	if($lolos)
	{ 
		
		$drop = DropMemory($tblnameA);    
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{ 
			$addQuery = GetArticleGroupCategory($array_data,$tb_category_data);
			$s_createA = " CREATE TABLE ".$tblnameA." ENGINE=MEMORY ".$addQuery." ";  
			$createA = mysql_query($s_createA);

			//echo $s_createA."<hr>";
			if(!$createA)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}  
	}
	
	// STEP B
	if($lolos)
	{ 
		
		$drop = DropMemory($tblnameB);    
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{  
			$best =  $array_data['best'] - 1;
			$s_createB = " CREATE TABLE ".$tblnameB." ENGINE=MEMORY SELECT * FROM ".$tblnameA."  LIMIT ".$best." ; ";  
			$createB = mysql_query($s_createB); 
			//echo $s_createB."<hr>";;
			if(!$createB)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."] {".$s_create."} ".mysql_error();
			}
		}  
	}
	// STEP C
	if($lolos)
	{
		
		$drop = DropMemory($tblnameC);    
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{ 
			$time_frame =  $array_data['time_frame'];
			
			$select = 'category_id, datee, COUNT(article_id) AS jum_artikel';
			$group = 'category_id, datee';
			$order = 'category_id ASC';
			// JIKA PAST YEAR
			if($time_frame > 350)
			{
				$select = 'category_id, LEFT(datee,7) AS datee, COUNT(article_id) AS jum_artikel';
				$group = 'category_id, YEAR(datee), MONTH(datee)';
			} 
			$addQuery = GetArticleGroupCategory($array_data,$tb_category_data,$select,$group,$order);
			$s_createC = " CREATE TABLE ".$tblnameC." ENGINE=MEMORY ".$addQuery." ";  
			$createC = mysql_query($s_createC); 
			//echo $s_createC."<hr>";;	
			if(!$createC)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}   
	}
	// STEP D
	if($lolos)
	{
		 
		$drop = DropMemory($tblnameD);    
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{   
			$s_createD = " CREATE TABLE ".$tblnameD." ENGINE=MEMORY  "
			." SELECT * FROM ".$tblnameC." "
			." WHERE category_id IN ( SELECT category_id FROM ".$tblnameB.") ORDER BY datee ASC;  ";  
			$createD = mysql_query($s_createD); 
			//echo $s_createD."<hr>";;		
			if(!$createD)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}  
	}
	
	// STEP E
	if($lolos)
	{
		$s_insert = "INSERT INTO ".$tblnameD." "
			." SELECT 'Others', datee, SUM(jum_artikel) FROM ".$tblnameC." "
			." WHERE category_id NOT IN ( SELECT category_id FROM ".$tblnameB.") "
			." GROUP BY datee ORDER BY datee;	";  
		$q_insert = mysql_query($s_insert); 
				
		if(!$q_insert)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		} 
	}
	
	//STEP G
	if($lolos)
	{ 
		$s_storeproc = " CALL spp_xtab('datee', 'datee', "
		." 'FROM ".$tblnameD."  ', 'jum_artikel', 'category_id', "
		." 'FROM ".$tblnameD." group by  category_id'); ";
		$q_storeproc = mysql_query($s_storeproc); 
   

		//echo $s_storeproc;
		if(!$q_storeproc)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		else
		{
			$r_query = mysql_num_rows($q_storeproc);
			if($r_query > 0)
			{
				while($d_query = mysql_fetch_assoc($q_storeproc))
				{
					$hasil[] = $d_query;
				}
			}
			else
			{
				$how = false;
				$result["code"] 	= "92"; 	
				$result["message"] 	= "No Data Display" ;
			}
			mysql_free_result($q_storeproc);
		} 
		
   
	}
	
	//hapus kembali 
	mysql_close();
	$conn2 = connectToDB(_DB_HOST_,_DB_USER_,_DB_PASS_,_DB_NAME_);
	$allTable = "`".$tblnameA."`,`".$tblnameB."`,`".$tblnameC."`,`".$tblnameD."`";
	$dropA = DropMemory($allTable); 
	if(!$dropA)
	{ 	
		$result["notice"] 	= "Error Database [".__LINE__."]".mysql_error();
	}  
		 
	
	if($lolos)
	{ 
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success"; 
		$result["data"] 	= $hasil;
		
	} 

	
?>
