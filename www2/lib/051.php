<?php
// 050
// Query to get tone group by issue_id
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id
// $time_frame => 7
	$lolos = true;
	$arr_parrameters = array("category_id","media_id","best","time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['best']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	$tblnameA = "all_".$a."cat_".$rand_time."_".$uid;
	if($lolos)
	{
		$drop = DropMemory($tblnameA);  
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{ 
			$tb_issue_data = _DB_NAME_.'.tb_issue_data';
			$select = 'issue_id, COUNT(article_id) as jum_artikel';
			$group = 'issue_id';
			$order = 'jum_artikel DESC'; 
			$addQuery = GetArticleGroupCategory($array_data,$tb_issue_data,$select,$group,$order); 
			$s_create = " CREATE TABLE ".$tblnameA." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}		 
		
	}
	 
	if($lolos)
	{  
		
		if($lolos)
		{
			$best = ($array_data['best'] < 5 ) ? 5 : $array_data['best'];
			$best = $best - 1;
			$addQuery = "SELECT issue_id FROM ".$tblnameA." LIMIT ".$best; 
			$getMmry = GetQuery($addQuery);
			
			if($getMmry[0])
			{
				$top_cat = $getMmry[1];
				$lolos = $getMmry[0];
			}
			else
			{
				$result = $getMmry[1];
				$lolos = $getMmry[0];  	
			} 
		}	 
		
	}
	
	$tblnameC = "tone_all_".$a."_".$rand_time."_".$uid;
	if($lolos)
	{
		$drop = DropMemory($tblnameC);   
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{  
			$select = 'issue_id,  tone, COUNT(article_id) AS jum_tone';
			$group = 'issue_id, tone';
			$order = 'issue_id ASC'; 
			$addQuery = GetArticleGroupCategory($array_data,$tb_issue_data,$select,$group,$order); 
			$s_create = " CREATE TABLE ".$tblnameC." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}		 
		
	}
	
	
	
	$tblnameD = "tone_".$a."_pivot_".$rand_time."_".$uid;
	if($lolos)
	{
		$drop = DropMemory($tblnameD);   
		if(!$drop)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		
		if($lolos)
		{   
		 
			$_issue_id = "";
			foreach($top_cat as $k => $v)
			{
				$_issue_id .= "'".$v['issue_id']."',";
			}
			$_issue_id = substr($_issue_id,0,-1); 
			//echo $category_id;
			$addQuery = "SELECT issue_id,  tone,  jum_tone "
						." FROM ".$tblnameC." "
						." WHERE issue_id  IN (".$_issue_id.") "
						." GROUP BY issue_id, tone "
						." ORDER BY issue_id; "; 
			$s_create = " CREATE TABLE ".$tblnameD." ENGINE=MEMORY ".$addQuery." "; 
			$create = mysql_query($s_create); 
					
			if(!$create)
			{
				$lolos = false;
				$result["code"] 	= "06"; 	
				$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
			}
		}		 
		
	} 
	
	if($lolos)
	{  
		$s_insert = "INSERT INTO ".$tblnameD." "
			." SELECT -1,  tone, SUM(jum_tone) as total FROM ".$tblnameC." "
			." WHERE issue_id NOT IN (".$_issue_id.") GROUP BY tone" ; 
		$q_insert = mysql_query($s_insert); 
				
		if(!$q_insert)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		} 
	}
	
	
	if($lolos)
	{
		/*
		CALL spp_xtab('tone', 'tone', 'FROM tone_pivot', 'jum_tone', 'category_id', 
		'FROM tone_pivot group by  category_id');*/
		//$MediaData = GetAllMediaName();
		$s_storeproc = " CALL spp_xtab('tone', 'tone', "
		." 'FROM ".$tblnameD." limit 60', 'jum_tone', 'issue_id', "
		." 'FROM ".$tblnameD." group by  issue_id'); ";
		$q_storeproc = mysql_query($s_storeproc); 
	  
		if(!$q_storeproc)
		{
			$lolos = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."]".mysql_error();
		}
		else
		{
			$r_query = mysql_num_rows($q_storeproc);
			if($r_query > 0)
			{
				while($d_query = mysql_fetch_assoc($q_storeproc))
				{
					$hasilx[] = $d_query;
				}
				$hasil = $hasilx;
				/*foreach($hasilx as $xxxx => $row)
				{
					$media_name = ( array_key_exists($row['media_id'],$MediaData ) ) ? $MediaData[$row['media_id']] : ''  ;
					$hasil[$xxxx]['media_name'] = $media_name;
					unset($hasil[$xxxx]['media_id']);
				}*/
			}
			else
			{
				$how = false;
				$result["code"] 	= "08"; 	
				$result["message"] 	= "No Data Display" ;
			}
			mysql_free_result($q_storeproc);
		} 
		
		
	} 
	
	
	
	//hapus kembali 
	mysql_close();
	$conn2 = connectToDB(_DB_HOST_,_DB_USER_,_DB_PASS_,_DB_NAME_);
	$allTable = "`".$tblnameA."`,`".$tblnameC."`,`".$tblnameD."`";
	$drop = DropMemory($allTable); 
	if(!$drop)
	{ 	
		$result["notice"] 	= "Error Database [".__LINE__."]".mysql_error();
	}
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $hasil;
	} 

	
?>
