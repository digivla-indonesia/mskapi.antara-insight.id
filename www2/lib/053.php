<?php  
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id
// $time_frame => 7
	$lolos = true;
	$arr_parrameters = array("category_id","media_id","time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	 	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	$active_ctg_id = array();
	if($lolos)
	{
		$str_category_id = "";
		$category_id 	= $array_data['category_id'];
		$time_frame 	= $array_data['time_frame'];
		$date_from	 	= $array_data['date_from'];
		$date_to	 	= $array_data['date_to'];
		foreach($category_id as $k => $v)
		{
			$str_category_id .= "'".$v."',";
		}	
		$str_category_id = substr($str_category_id,0,-1);
		
		$data_media_id = $array_data['media_id'];
		$str_media_id = "";
		foreach($data_media_id as $k => $mdid)
		{ 
			$str_media_id .= "".$mdid.",";
		}
		$str_media_id = substr($str_media_id,0,-1);
		
		if($str_category_id !== "")
		{ 
			$media_date = GetMediaDate($time_frame,$date_from,$date_to);
			
			$s_activeCtg = "SELECT tone, COUNT(article_id) as total FROM "
			." ".$tb_category_data." WHERE category_id  IN (".$str_category_id.") "
			." ".$media_date." "
			." AND media_id IN (".$str_media_id.") GROUP BY  tone "; 
			$d_activeCtg = GetQuery($s_activeCtg);		
			if($d_activeCtg[0])
			{
				$hasil 		= $d_activeCtg[1];
				$total_ctg 	= $d_activeCtg[2];
				$lolos 		= $d_activeCtg[0];
			}
			else
			{
				$result 	= $d_activeCtg[1];
				$total_ctg 	= $d_activeCtg[2];
				$lolos 		= $d_activeCtg[0];  	
			}
		}
		else
		{
			$lolos = false;
			$result["code"] 	= "03"; 	
			$result["message"] 	= "Wrong Data Parameter";
		}

	}   
	 
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $hasil;
	} 

	
?>
