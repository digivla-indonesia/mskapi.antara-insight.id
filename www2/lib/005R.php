<?php
// function 005 Query to get Current and past news (3 days)
// Query to show the amount of input articles group by category in a range of a date
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id 


	$lolos = true;
  $arr_parrameters="";
	$arr_parrameters = array( "category_id","media_id", "limit","time_frame","date_from","date_to");

	//result $arr_parrameters[1];
 
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete [". __LINE__ ."]";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete [". __LINE__ ."]";
		}
	}
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	 
	if($lolos)
	{
		if(!is_numeric($array_data['limit']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete [". __LINE__ ."]";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete [". __LINE__ ."]";
		}
	}
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	// STEP A
	
	
	
	if($lolos)
	{ 
		$category_id = "";
		foreach($array_data['category_id'] as $k => $v)
		{
			$category_id .= "'".$v."',";
		}
		$category_id = substr($category_id,0,-1);
		
		$media_id = "";
		foreach($array_data['media_id'] as $k => $v)
		{
			$media_id .= "".$v.",";
		}
		$media_id = substr($media_id,0,-1);
		 
		 
		 
	}
	$hasil = array();
	// STEP B
	if($lolos)
	{  
		
		if($lolos)
		{			
			$time_frame = $array_data['time_frame'];
			$date_from = $array_data['date_from'];
			$date_to = $array_data['date_to'];
			$tanggale = GetMediaDate($time_frame,$date_from,$date_to);
			
			/*$s_Query = " SELECT article_id , category_id, datee, media_id , tone "
						." FROM ".$tb_category_data." "
						." WHERE category_id IN(".$category_id.") "
						." ".$tanggale." "
						." AND media_id IN (".$media_id.") "
						." ORDER BY datee  DESC; ";*/
			$select = 'article_id, tone';
			$group = 'article_id';
			$order = 'datee,article_id DESC';
			$s_Query = GetArticleGroupCategory($array_data,$tb_category_data,$select,$group,$order);
			$getMmry = GetQuery($s_Query);
			
			if($getMmry[0])
			{
				$hasil = $getMmry[1];
				$lolos = $getMmry[0];
			}
			else
			{
				$result = $getMmry[1];
				$lolos = $getMmry[0];
			} 
		}
		 
	}
	
	$title = $media = $d_title = $d_media = array(); 
	 
	
	$theData = array();
	if($lolos)
	{
		 
		$arrTones = array();
		$articles = '';
		foreach($hasil as $k => $v)
		{  
			$article_id = $v['article_id']; 
			$arrTones[$article_id] =$v['tone'];
			$articles .= $article_id.',';
		}
		$articles = substr($articles,0,-1);
		
		$limit = ( $array_data['limit'] == 0 ) ? 20 : $array_data['limit'] ; 
		$s_select = "SELECT *"
					." FROM `db_msk_api`.tb_articles_radio "
					//." WHERE article_id IN (".$articles.") "
					." ORDER BY datee DESC  LIMIT  ".$limit.";	";
             
             
             
             
		$getMmry = GetQuery($s_select);
		if($getMmry[0])
		{
			$dataarticle = $getMmry[1];
			$lolos = $getMmry[0]; 
			
			$s_select = GetMediaName($dataarticle,'media_id'); 
			$getMediaData = GetQuery($s_select); 
	 		if($getMediaData[0])
			{
				$media = $getMediaData[1];
				$lolos = $getMediaData[0];
				
				if(count($media) > 0)
				{
					foreach($media as $k => $v)
					{
						$d_media[$v['media_id']] = $v['media_name'];
					}
				}
			}
			
			$theData = array();
			foreach($dataarticle as $k => $v)
			{ //article_id, title, datee, media_id , journalist, content, circulation, mmcol
				$article_id = $v['article_id'];
				$title = $v['title'];
				$datee = $v['datee'];
        $timee = $v['timee'];
        $duration = $v['duration'];
				$media_id = $v['media_id'];
				$journalist = $v['journalist'];
				$content = $v['content'];
				//$circulation = $v['circulation'];
				//$mmcol = $v['mmcol'];
				//$page = $v['page'];
				$filee = $v['filee'];
				$media_name = $d_media[$media_id]; 
				 
			$content = str_replace(array("\r\n", "\r", "\n"), "<br />", $content); 
  	  $content = str_replace(array("\\r\\n", "\\r", "\\n"), "<br />", $content); 
      $content = stripslashes(	$content);
      $content = str_replace("<br>", "", $content);
       
				$theData[] = array(
					"article_id" => $article_id,
					"title" => $title,
					"datee" => $datee,
          "timee" => $timee,
          "duration" => $duration,
					"tone" => $arrTones[$article_id],
					"media_id" => $media_id,
					"media_name" => $media_name,
					"journalist" => $journalist, 
					"content" => $content, 
					//"circulation" => $circulation, 
					//"mmcol" => $mmcol, 
					//"page" => $page, 
					"filee" => $filee, 
				); 
			}
		}
		else
		{
			$result = $getMmry[1];
			$result["query1"] 	= $s_Query;
			$result["query2"] 	= $s_select;
			$lolos = $getMmry[0];
		}
	} 
	
	
	 
	
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
		//$result["articles"] 	= $articles;
		$result["query1"] 	= $s_Query;
		$result["query2"] 	= $s_select;
	} 

	
?>
