<?php
// 010
// Query to get the advertising value (Advalue)
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id
// $time_frame => 7
// $exposure => 25 (Numeric)
// $valrate => 1,1.5,3
	$lolos = true;
	$arr_parrameters = array( "category_id", "valrate", "exposure", "time_frame","date_from","date_to");
	
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['valrate']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['exposure']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	 
	if($lolos)
	{  
		 
		
		$category_id = "";
		foreach($array_data['category_id'] as $k => $v)
		{
			$category_id .= "'".$v."',";
		}
		$category_id = substr($category_id,0,-1);
		
		$valrate = $array_data['valrate'];
		$exposure = $array_data['exposure'];
		settype($exposure,"INT");
		$add_exposure = "";
		if($exposure > 0)
		{
			$add_exposure = " * ".$exposure." ";
		}
		
		$time_frame =  $array_data['time_frame'];
		$date_from =  $array_data['date_from'];
		$date_to =  $array_data['date_to'];
		
		$queryDatee = GetMediaDate($time_frame,$date_from,$date_to);
		
		$s_select = " SELECT category_id, datee, (SELECT SUM(advalue_bw) * ".$valrate." ".$add_exposure." ) advertising_value "
					." FROM ".$tb_category_data." "
					." WHERE category_id IN (".$category_id.") "
					." ".$queryDatee." " 
					." AND advalue_bw > 0 "
					." GROUP BY datee "; 
		$q_query = mysql_query($s_select); 
		
		if($q_query)
		{
			$r_query = mysql_num_rows($q_query);
			if($r_query > 0)
			{
				while($d_query = mysql_fetch_assoc($q_query))
				{
					$hasil[] = $d_query;
				}
			}
			else
			{
				$how = false;
				$result["code"] 	= "08"; 	
				$result["message"] 	= "No Data Display" ;
			}
		}
		else
		{
			$how = false;
			$result["code"] 	= "06"; 	
			$result["message"] 	= "Error Database [".__LINE__."] ".mysql_error();
		} 
		
	} 
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $hasil;
	} 

	
?>
