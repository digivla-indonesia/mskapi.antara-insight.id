<?php
// 015 Query to get the today’s news  
	$lolos = true; 
	$arr_parrameters = array("keywords","category","media_id","time_frame","date_from","date_to","best");  
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['keywords']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	
	if($lolos)
	{
		if(!is_array($array_data['category']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_numeric($array_data['time_frame']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	 
	if($lolos)
	{
		if(!is_numeric($array_data['best']))
		{
			$lolos = false;
			$result["code"] 	= "10"; 	
			$result["message"] 	= "Wrong Parameter Values";
		}
	}
	 
	
	if($lolos)
	{
		if($array_data['time_frame'] == "0")
		{
			// CHECKING DATE FROM
			if($lolos)
			{
				$cekDateFrom = valid_date($array_data['date_from']);
				
				
				if($cekDateFrom === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
			// CHECKING DATE TO
			if($lolos)
			{
				$cekDateTo	 = valid_date($array_data['date_to']);
				if($cekDateTo === false)
				{
					$lolos = false;
					$result["code"] 	= "11"; 	
					$result["message"] 	= "Wrong Date Values";
				}
			}
		}
	}
	
	
	// STEP A 
	if($lolos)
	{  	 
		$data_temp 		= array();
		$keywords 		= $array_data['keywords'];
		
		$date_from 		= $array_data['date_from'];
		$date_to 		= $array_data['date_to'];
		$time_frame 	= $array_data['time_frame'];
		$best		 	= $array_data['best'];
		
		if($best < 5)
		{
			$best = 5;
		}
		
		$q_media_id = "";
		foreach($array_data['media_id'] as $k => $v)
		{
			$q_media_id .= "".$v.",";
		}
		$q_media_id = substr($q_media_id,0,-1);
		$MediaGraph = array();
		
		
		$mediadate 		= GetMediaDate($time_frame,$date_from,$date_to,'tgl');
		
		
		
		$total_row = 0;
		foreach($keywords as $ii => $textkeyword)
		{
			$categoryna 		= $array_data['category'][$ii];
			
			$cekExcludeWord 	= GetWordExclude($textkeyword);
			if( $cekExcludeWord )
			{
				$lolos = false;
				$result["code"] 	= "08"; 	
				$result["message"] 	= "Sorry, keyword '".$textkeyword."' bloked." ;
			} 
			
			if($lolos)
			{
				$mode = "extended"; // tadinya "any"
				$arrSyntax  = array('|', ':', '"', '-','+','!','@','>','<','*');
				if (strposa($textkeyword, $arrSyntax, 1)) {
					$mode =  'extended';
				}  
				
				$textkeyword = str_replace("'",'"',$textkeyword);
							
				$addQuery 		= "SELECT id as article_id , media_id  "
								." FROM `"._DB_NAME_."`.tb_cari "
								." WHERE (QUERY ='".$textkeyword.";limit=10000000;mode=".$mode.";sort=attr_desc:tgl') "
								." ".$mediadate." "
								." AND media_id IN (".$q_media_id.") ; ";
				//echo $addQuery;
				
				
				$getMmry = GetQuery($addQuery);		
				if($getMmry[0])
				{
					$hasilA 	= $getMmry[1];
					$total_row 	= $getMmry[2];
					$lolos 		= $getMmry[0];
				}
				else
				{
					$result 	= $getMmry[1];
					$total_row 	= $getMmry[2];
					$lolos 		= $getMmry[0];  	
				} 
			}
			
			//$data_temp[$categoryna]['total_row']	= $total_row; // stored in array to result
			
			$data_temp["graph1"][] = array(
				"category" => $categoryna,
				"total" => $total_row,
			);  
			
			if($lolos)
			{
				$q_article_id = "";
				foreach($hasilA as $k => $v)
				{
					$q_article_id .= "".$v['article_id'].",";
				}
				$q_article_id = substr($q_article_id,0,-1); 			
				
				
				$addQuery 		= "SELECT article_id,media_id ,content, title ,datee ,file_pdf "
								 ." FROM "._DB_NAME_.".tb_articles "
								 ."	WHERE article_id IN (".$q_article_id.") ";
							
				$getMmry = GetQuery($addQuery);		
				if($getMmry[0])
				{
					$hasilB 	= $getMmry[1];
					$total_rowB = $getMmry[2];
					$lolos 		= $getMmry[0];
				}
				else
				{
					$result 	= $getMmry[1];
					$total_rowB = $getMmry[2];
					$lolos 		= $getMmry[0];  	
				} 
			}
			
			$datena = $date_awal = $date_akhir = ""; 
			if($lolos)
			{ 
				$s_select = GetMediaName($hasilB,'media_id'); 
				$getMmry = GetQuery($s_select);
				
				if($getMmry[0])
				{
					$media = $getMmry[1];
					$lolos = $getMmry[0];
					
					if(count($media) > 0)
					{
						foreach($media as $k => $v)
						{
							$d_media[$v['media_id']] = $v['media_name'];
						}
					} 
					
					
					foreach($hasilB as $k => $v)
					{  
						$media_id = $v['media_id'];  
						$media_name = $d_media[$media_id];   
						$MediaGraph[$media_name][] = $media_id;
					} 
					
				}
				else
				{
					$result = $getMmry[1];
					$lolos = $getMmry[0];
				}  
				
			} 
			$lolos = true;
		}
		
		if($lolos)
		{
			$best_media = array();
			foreach($MediaGraph as $kx => $vx)
			{ 
				//$data_temp[$categoryna][$kx]		= count($vx);
				$best_media[$kx] = count($vx);
			} 
			arsort($best_media);
			$w = 0;
			$other = 0 ;
			foreach($best_media as $kxx => $vxx)
			{
				if($w < $best)
				{
					$data_temp["graph2"][] = array(
						"media" => $kxx ,
						"total" => $vxx ,
					); 
				}
				else
				{
					$other = $other + $vxx ;
				}
				$w++;
			}
			$data_temp["graph2"][] = array(
						"media" => "Other" ,
						"total" => $other  ,
					);
			 
		} 
	} 
	  
	 
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $data_temp;
	}
	
	

?>
