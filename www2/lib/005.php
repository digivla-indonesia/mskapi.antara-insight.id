<?php
// function 005 Query to get Current and past news (3 days)
// Query to show the amount of input articles group by category in a range of a date
// $category_id => '@category_id1', '@category_id2'
// $media_id => @media_id, @media_id 
	$lolos = true;
	$arr_parrameters = array( "category_id","media_id");
	
	
	if(!check_data_params($array_data,$arr_parrameters))
	{
		$lolos = false;
		$result["code"] 	= "07"; 	
		$result["message"] 	= "Parameter Not Complete";
	}
	
	if($lolos)
	{
		if(!is_array($array_data['category_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	if($lolos)
	{
		if(!is_array($array_data['media_id']))
		{
			$lolos = false;
			$result["code"] 	= "07"; 	
			$result["message"] 	= "Parameter Not Complete";
		}
	}
	
	// STEP A
	
	
	
	if($lolos)
	{ 
		$category_id = "";
		foreach($array_data['category_id'] as $k => $v)
		{
			$category_id .= "'".$v."',";
		}
		$category_id = substr($category_id,0,-1);
		
		$media_id = "";
		foreach($array_data['media_id'] as $k => $v)
		{
			$media_id .= "".$v.",";
		}
		$media_id = substr($media_id,0,-1);
		 
		
		if($lolos)
		{			
			$s_create = " SELECT DISTINCT  datee FROM ".$tb_category_data."  "
						." WHERE category_id  IN(".$category_id.") "
						." AND media_id IN (".$media_id.") "
						." ORDER BY datee DESC LIMIT 10;	";
			$getMmry = GetQuery($s_create);
			
			if($getMmry[0])
			{
				$hasil = $getMmry[1];
				$lolos = $getMmry[0];
			}
			else
			{
				$result = $getMmry[1];
				$lolos = $getMmry[0];
			} 
		}
		 
	}
	
	// STEP B
	if($lolos)
	{  
		
		if($lolos)
		{			
			$datee = ""; 
			foreach($hasil as $k => $v)
			{
				$datee .= "'".$v['datee']."',";
			}
			$datee = substr($datee,0,-1); 
			
			$s_Query = " SELECT article_id , category_id, datee, media_id  "
						." FROM ".$tb_category_data." "
						." WHERE category_id IN(".$category_id.") "
						//." AND datee IN( ".$datee.") "
						/*TEST HIGHLIGHT*/
						." AND datee IN('".$array_data['highest']."') "
						/*TEST HIGHLIGHT*/
						." AND media_id IN (".$media_id.") "
						." GROUP BY datee, category_id, media_id , article_id ORDER BY datee DESC; "; 
			$getMmry = GetQuery($s_Query);
			
			if($getMmry[0])
			{
				$hasil = $getMmry[1];
				$lolos = $getMmry[0];
			}
			else
			{
				$result = $getMmry[1];
				$lolos = $getMmry[0];
			} 
		}
		 
	}
	
	$title = $media = $d_title = $d_media = array();
	// STEP C
	if($lolos)
	{ 
		$s_select = GetTitle($hasil); 
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$title = $getMmry[1];
			$lolos = $getMmry[0];
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	}
	
	if(count($title) > 0)
	{
		foreach($title as $k => $v)
		{
			$d_title[$v['article_id']] = $v['title'];
		}
	}
	
	// STEP D
	if($lolos)
	{ 
		$s_select = GetMediaName($hasil,'media_id'); 
		$getMmry = GetQuery($s_select);
		
		if($getMmry[0])
		{
			$media = $getMmry[1];
			$lolos = $getMmry[0];
		}
		else
		{
			$result = $getMmry[1];
			$lolos = $getMmry[0];
		} 
	} 
	
	if(count($media) > 0)
	{
		foreach($media as $k => $v)
		{
			$d_media[$v['media_id']] = $v['media_name'];
		}
	} 
	
	$theData = array();
	foreach($hasil as $k => $v)
	{ 
		$datee = $v['datee'];
		$article_id = $v['article_id'];
		$article_title = $d_title[$v['article_id']];
		$category_id = $v['category_id'];
		$media_name = $d_media[$v['media_id']]; 
		$theData[$datee][$category_id][$media_name][$article_id] = $article_title; 
		
		ksort($theData[$datee][$category_id]);
		ksort($theData[$datee][$category_id][$media_name]);
	}
	
	
	if($lolos)
	{
		$result["code"] 	= "00"; 	
		$result["message"] 	= "Success";
		$result["data"] 	= $theData;
	} 

	
?>
